package gov.doe.jgi

import gov.doe.jgi.config.Organization
import gov.doe.jgi.cv.PlatformTypeCv
import gov.doe.jgi.cv.SequencerModelTypeCv
import gov.doe.jgi.domain.FinalDelivProduct
import gov.doe.jgi.domain.LibraryCreationQueue
import gov.doe.jgi.domain.LibraryCreationSpecs
import gov.doe.jgi.domain.QcType
import gov.doe.jgi.domain.RunMode
import gov.doe.jgi.mapper.RunModeCvMapper
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.utils.ClarityDataService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class ClarityDataServiceSpec {
    static final Logger log = LoggerFactory.getLogger(ClarityDataServiceSpec.class)

    @Autowired
    ClarityDataService clarityDataService
    @Autowired
    NodeManager nodeManager

    @BeforeEach
    void setup() {
        nodeManager = nodeManager.isClosed ? new NodeManager(nodeManager.nodeConfig) : nodeManager
    }
    @AfterEach
    void cleanup() {
        nodeManager.close()
    }

    @Disabled @Test
    void "retrieve process types for process ids"() {
        given:
        List<String> processIds = ['24-26205']
        when:
        Map<String, String>  typeIds = clarityDataService.findProcessTypeForId(processIds)
        then:
        assert typeIds
        log.debug "$typeIds"
    }

    @Test
    void "retrieve run modes"() {
        given:
        when:
        Map<Long, RunMode> runModes = clarityDataService.fetchRunModes()
        then:
        runModes
    }

    @Test
    void "retrieve all run modes"() {
        given:
        when:
        Collection<RunMode> runModes = RunMode.list()
        then:
        runModes
    }

    @Test
    void "retrieve qc types"() {
        given:
        when:
        Map<Long, QcType> qcTypes = clarityDataService.fetchQcTypes()
        then:
        qcTypes
    }

    @Disabled @Test
    void "retrieve input analytes ids for process id"() {
        given:
        String prcLimsId = '122-1111299'
        String outLimsId = '2-4317727'
        when:
        List<String> analytesIDs = clarityDataService.getInputAnalytesIdsForProcessIdAndOutPutArtId(prcLimsId, outLimsId)
        then:
        analytesIDs
    }

    @Test
    void "retrieve library creation spec by id"() {
        given:
        Long specId = 79
        when:
        LibraryCreationSpecs specs = clarityDataService.getLibraryCreationSpecsById(specId)
        then:
        specs
    }

    @Test
    void "retrieve library creation spec by name"() {
        given:
        String  specName = 'Illumina RNASeq w/PolyA Selection'
        when:
        LibraryCreationSpecs specs = clarityDataService.getLibraryCreationSpecsByName(specName)
        then:
        specs
    }

    @Test
    void "retrieve library creation queue by id"() {
        given:
        Long queueId = 64
        when:
        LibraryCreationQueue libraryCreationQueue = clarityDataService.getLibraryCreationQueue(queueId)
        then:
        libraryCreationQueue
        log.debug("libraryCreationQueue for id [$queueId]: $libraryCreationQueue")
    }
    @Test
    void "test getRunModes"() {
        when:
            Collection<RunMode> result = RunMode.getActiveRunModes()
        then:
            assert result.size() > 0
            result.findAll{
                it.active && it.organizationName == Organization.JGI.name
            }.each{
                assert it.id
                assert it.runMode
                assert it.platform in PlatformTypeCv.values().collect{it.value}
                assert it.sequencerModel in SequencerModelTypeCv.values().collect{it.value}
            }
        when:
            RunMode runModeId52 = RunMode.read(52)
        then:
            assert runModeId52.runMode == 'Illumina NovaSeq SP 2 X 150'
            assert runModeId52.active
            assert runModeId52.organizationName == Organization.JGI.name
            assert runModeId52.platformId == 1
            assert runModeId52.platform == PlatformTypeCv.ILLUMINA.value
            assert runModeId52.sequencerModelId == 17
            assert runModeId52.sequencerModel == SequencerModelTypeCv.NOVASEQ_SP.value
            //assert runModeId52.workflowId == 401
            assert runModeId52.readLengthBp == 150
            assert runModeId52.readTotal == 2

            assert RunModeCvMapper.toRunModeCv(runModeId52).sequencerModel.isIlluminaNovaSeq
    }
    @Test
    void "test getRunModesByOrganization"() {
        when:
            Collection<RunMode> runModes = clarityDataService.getRunModes()
            List<RunMode> runModesByJgi = clarityDataService.getRunModesByOrganization(Organization.JGI)
            List<RunMode> runModesByHudsonAlpha = clarityDataService.getRunModesByOrganization(Organization.HUDSON_ALPHA)
        then:
            assert runModes.size() > 0
            List<RunMode> findAllJgi = runModes.findAll{it.organizationName == Organization.JGI.name}
            assert findAllJgi.size() == runModesByJgi.size()
            runModesByJgi.each{
                assert it.organizationName == Organization.JGI.name
            }
            List<RunMode> findAllHudsonAlpha = runModes.findAll{it.organizationName == Organization.HUDSON_ALPHA.name}
            assert findAllHudsonAlpha.size() == runModesByHudsonAlpha.size()
            runModesByHudsonAlpha.each{
                assert it.organizationName == Organization.HUDSON_ALPHA.name
            }
    }

    static final String queueName = "Illumina Low Input RNASeq w/PolyA Selection"
    @Test
    void "test getLibraryCreationSpecs By Id"() {
        setup:
            Long specsId = 4
        when:
            LibraryCreationSpecs specsById = clarityDataService.getLibraryCreationSpecsById(specsId)
            LibraryCreationQueue queueDto4 = specsById.lcQueues.find{it.id == 4}
            LibraryCreationQueue queueDto66 = specsById.lcQueues.find{it.id == 66}
        then:
            assert specsById
            assert specsById.libraryCreationSpecs == queueName
            assert specsById.organizationName == Organization.JGI as String
            assert specsById.active
            assert specsById.lcQueues.size() == 2
            assert queueDto4.id == 4
            assert queueDto4.libraryCreationQueue == "$queueName, Tubes"
            assert queueDto66.id == 66
            assert queueDto66.libraryCreationQueue == "$queueName, Plates"
            assert queueDto4.tubeTargetMassLibTrialNg == 200.0
            assert queueDto4.plateTargetMassLibTrialNg == null
            assert queueDto4.tubeTargetVolLibTrialUl == 20.0
            assert queueDto4.plateTargetVolLibTrialUl == null
            assert queueDto66.tubeTargetMassLibTrialNg == null
            assert queueDto66.plateTargetMassLibTrialNg == 200.0
            assert queueDto66.tubeTargetVolLibTrialUl == null
            assert queueDto66.plateTargetVolLibTrialUl == 20.0
    }

    @Test
    void "test getLibraryCreationSpecs for JGI" () {
        when:
            List<LibraryCreationSpecs> result = clarityDataService.getLibraryCreationSpecs(Organization.JGI)
        then:
            assert result.size() > 0
            assert result.collect { it.id }.unique().size() == result.size()
            result.findAll {LibraryCreationSpecs lcs -> (lcs.active) }.each {LibraryCreationSpecs lcs ->
                assert lcs.libraryCreationSpecs
                assert lcs.lcQueues
                lcs.lcQueues.each { LibraryCreationQueue lcq ->
                    assert lcq.libraryCreationQueue
                }
            }
    }

    @Test
    void "test getLibraryCreationSpecs for Hudson alpha" () {
        when:
            List<LibraryCreationSpecs> result = clarityDataService.getLibraryCreationSpecs(Organization.HUDSON_ALPHA)
        then:
            assert result.size() > 0
            assert result.collect { it.id }.unique().size() == result.size()
            result.each { LibraryCreationSpecs lcs ->
                assert lcs.libraryCreationSpecs
                assert lcs.id
                assert lcs.organizationName == Organization.HUDSON_ALPHA.name
                assert !lcs.active
            }
    }

    @Test
    void "test getLibraryCreationQueue"() {
        setup:
            Long queueId =27
        when:
            LibraryCreationQueue queueId27 = clarityDataService.getLibraryCreationQueue(queueId)
        then:
            assert queueId27?.libraryCreationQueue == 'PacBio 2kb'
        when:
            LibraryCreationQueue queueId4 = clarityDataService.getLibraryCreationQueue(4)
        then:
            assert queueId4?.libraryCreationQueue == "$queueName, Tubes"
        when:
            LibraryCreationQueue queueId66 = clarityDataService.getLibraryCreationQueue(66)
        then:
            assert queueId66?.libraryCreationQueue == "$queueName, Plates"
    }

    @Test
    void "test getQcTypes"() {
        when:
            def qcTypes = clarityDataService.fetchQcTypes().values()
            QcType qcType0 = QcType.read(0)
            QcType qcType9 = QcType.read(9)
        then:
            assert qcTypes.size()
            qcTypes.each{
                assert it.id >= 0
                assert it.qcType
            }
            assert qcType0.qcType == 'None'
            assert qcType0.active == 'Y'
            assert qcType9.qcType == 'Other+Purity'
            assert qcType9.active == 'N'
    }

    @Test
    void "retrieve FinalDelivProduct by id"() {
        given:
        Serializable id = 7
        when:
        FinalDelivProduct entity = FinalDelivProduct.read(id)
        FinalDelivProduct expectedEntity = new FinalDelivProduct(
                id:id,
                finalDelivProductName: 'Plant/Other Eukaryote Annotated Standard Draft+',
                active: false
        )
        then:
        entity
        log.info("$entity")
        assert entity == expectedEntity
    }
}
