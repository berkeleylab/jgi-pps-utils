package gov.doe.jgi

import gov.doe.jgi.claritylims.ClarityLimsService
import gov.doe.jgi.claritylims.ClarityQueueDto
import gov.doe.jgi.config.ClarityWorkflow
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class ClarityLimsServiceSpec {
    @Autowired
    ClarityLimsService clarityLimsService

   @Test
    void "test getClarityQueueDtoList"() {
        setup:
            def workflowNames = [
                    ClarityWorkflow.SAMPLE_QC_RNA.value,
                    ClarityWorkflow.ALIQUOT_CREATION_RNA.value
            ]
        when:
            List<ClarityQueueDto> queueDtoList = clarityLimsService.getClarityQueueDtoList(workflowNames)
            List<String> artifactIds = queueDtoList.collect{it.luid}
        then:
            assert artifactIds.size() > 0
            queueDtoList.each{assert it.workflowName in workflowNames}
    }
}