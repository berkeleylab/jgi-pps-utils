package gov.doe.jgi

import gov.doe.jgi.pi.pps.clarity_node_manager.util.NodeConfig
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.env.Environment
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

import javax.sql.DataSource
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="clarity")
class ClaritySettings implements DisposableBean {
    String url
    String user
    String password

    @Autowired
    Environment env

    @Bean(name = "ussDataSource")
    @Primary
    DataSource ussDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.url(env.getProperty("spring.datasource.url"))
        dataSourceBuilder.username(env.getProperty("spring.datasource.username"))
        dataSourceBuilder.password(env.getProperty("spring.datasource.password"))
        dataSourceBuilder.driverClassName(env.getProperty("spring.datasource.driverClassName"))
        DataSource source = dataSourceBuilder.build()
        return source
    }

    @Bean(name = "ussJdbcTemplate")
    NamedParameterJdbcTemplate ussJdbcTemplate(
            @Qualifier("ussDataSource") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds)
    }

    @Bean(name = "limsDataSource")
    @Qualifier("limsDataSource")
    DataSource limsDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.url(env.getProperty("limsDataSource.url"))
        dataSourceBuilder.username(env.getProperty("limsDataSource.username"))
        dataSourceBuilder.password(env.getProperty("limsDataSource.password"))
        dataSourceBuilder.driverClassName(env.getProperty("limsDataSource.driverClassName"))
        DataSource source = dataSourceBuilder.build()
        return source
    }

    @Bean(name = "limsJdbcTemplate")
    NamedParameterJdbcTemplate limsJdbcTemplate(
            @Qualifier("limsDataSource") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds)
    }


    final static int THREADS = 10
    static ExecutorService executorService = Executors.newFixedThreadPool(THREADS)

    NodeConfig toNodeConfig() {
        NodeConfig nodeConfig = new NodeConfig()
        nodeConfig.executorService = executorService
        nodeConfig.geneusUrl = url
        nodeConfig.geneusUser = user
        nodeConfig.geneusPassword = password
        nodeConfig.clarityDataSource = limsDataSource()
        nodeConfig
    }

    @Override
    void destroy() throws Exception {
        executorService.shutdownNow()
    }

}
