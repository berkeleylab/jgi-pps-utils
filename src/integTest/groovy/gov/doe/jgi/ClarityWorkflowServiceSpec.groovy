package gov.doe.jgi

import gov.doe.jgi.model.ClarityActiveWorkflow
import gov.doe.jgi.utils.ClarityWorkflowService
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class ClarityWorkflowServiceSpec {
    static final Logger log = LoggerFactory.getLogger(ClarityWorkflowServiceSpec.class)

    @Autowired
    ClarityWorkflowService clarityWorkflowService

    @Test
    void "getClarityWorkflows by process type"() {
        given:
            String processType = ProcessTypeTest.SM_SAMPLE_QC.value
        when:
        List<ClarityActiveWorkflow> activeWorkflows = clarityWorkflowService.getClarityWorkflows(processType)
            log.info "activeWorkflows: ${activeWorkflows*.name.join(', ')}"
        then:
            assert activeWorkflows.size() == 2
        when:
            processType = ProcessTypeTest.LC_LIBRARY_CREATION.value
            activeWorkflows = clarityWorkflowService.getClarityWorkflows(processType)
        then:
            assert activeWorkflows.size() > 50
            activeWorkflows.each{
                assert it.name.startsWith('LC ')
            }
    }
}
