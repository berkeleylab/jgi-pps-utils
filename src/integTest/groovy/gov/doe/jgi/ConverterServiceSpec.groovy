package gov.doe.jgi

import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.tlaconverter.ConverterRequestUnitDto
import gov.doe.jgi.tlaconverter.ConverterResponseUnitDto
import gov.doe.jgi.tlaconverter.ConverterService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class ConverterServiceSpec {
    static final String REAGENT_LABEL_PREFIX = 'SOW Item Id - '

    @Autowired ConverterService tlaConverterService
    @Autowired NodeManager nodeManager

    @BeforeEach
    void setup() {
        nodeManager = nodeManager.isClosed ? new NodeManager(nodeManager.nodeConfig) : nodeManager
    }
    @AfterEach
    void cleanup() {
        nodeManager.close()
    }

    def gthppLimsId = '2-3952063'
    //def gthpsLimsId = '2-3952064'
/*
REQUEST
https://ws-access-int.jgi.doe.gov/pps-logical-amount-units-converter
{
    "logical-amount-units      // [
        {
            "library-name": "GTHPP",
            "from-logical-amount": 0.25,
            "from-logical-amount-units": "M reads",
            "to-logical-amount-units": "Gb"
        }
    ]
}
RESPONSE
{
    "logical-amount-units": [
        {
            "index": 0,
            "sow-item-id": 335098,
            "library-name": "GTHPP",
            "from-logical-amount-units": "M reads",
            "from-logical-amount": 0.25,
            "to-logical-amount-units": "Gb",
            "to-logical-amount": 0.300000000,
            "target-logical-amount": 0.25,
            "logical-amount-units": "M reads",
            "run-mode": "PacBio Sequel 1 X 1200",
            "read-total": 1,
            "read-length-bp": 1200,
            "number-of-reads-bp": 250000,
            "genome-size-estimated-mb": null,
            "mappable-yield-rate": null
        }
    ]
}
 */
    @Test
    void "test convertUnits"() {
        setup:
            ConverterRequestUnitDto unit = new ConverterRequestUnitDto(
                    libraryName: 'GTHPP',
                    fromLogicalAmount: 0.25,
                    fromLogicalAmountUnits: 'M mappable reads',
                    toLogicalAmountUnits: ConverterService.GB
            )
        when:
            ConverterResponseUnitDto[] response = tlaConverterService.convertUnits([unit] as ConverterRequestUnitDto[])
            Long sowItemId = (nodeManager.getArtifactNode(gthppLimsId).reagentLabels[0] - REAGENT_LABEL_PREFIX) as Long
        then:
            assert response
            assert response.size() == 1
            response.each{
                    assert it.index == 0
                    assert it.sowItemId == sowItemId
                    assert it.targetLogicalAmount == 0.25
                    assert it.logicalAmountUnits == 'M mappable reads'
                    assert it.libraryName == "GTHPP"
                    assert it.fromLogicalAmount == 0.25
                    assert it.fromLogicalAmountUnits == 'M mappable reads'
                    assert it.toLogicalAmountUnits == tlaConverterService.GB
                    assert it.toLogicalAmount == 0.450000000
            }
    }

    def sowItems = [
            new ConverterRequestUnitDto(
                    libraryName: 'CWAXB',
                    toLogicalAmountUnits: ConverterService.GB
                    //sowItemId: 284967,
                    //targetLogicalAmount: 200,
                    //logicalAmountUnits: 'X'
            ),
            new ConverterRequestUnitDto(
                    sowItemId: 286187,
                    toLogicalAmountUnits: ConverterService.GB
                    //targetLogicalAmount: 6,
                    //logicalAmountUnits: 'Gb'
            ),
            new ConverterRequestUnitDto(
                    sowItemId: 286300,
                    toLogicalAmountUnits: ConverterService.GB
                    //targetLogicalAmount: 100,
                    //logicalAmountUnits: 'M mappable reads'
            ),
            new ConverterRequestUnitDto(
                    sowItemId: 286185,
                    toLogicalAmountUnits: ConverterService.GB
                    //targetLogicalAmount: 100,
                    //logicalAmountUnits: 'M reads'
            )
    ]
    @Test
    void "test convertUnits sow items"() {
        when:
            ConverterResponseUnitDto[] response = tlaConverterService.convertUnits(sowItems as ConverterRequestUnitDto[])
        then:
            assert response
            assert response.size() == 4
            ConverterResponseUnitDto responseUnitDto = response[0]
            assert responseUnitDto.index == 0
            assert responseUnitDto.sowItemId == 284967
            assert responseUnitDto.targetLogicalAmount == 200
            assert responseUnitDto.logicalAmountUnits == 'X'

            ConverterResponseUnitDto responseUnitDto1 = response[1]
            assert responseUnitDto1.index == 1
            assert responseUnitDto1.sowItemId == 286187
            assert responseUnitDto1.targetLogicalAmount == 6
            assert responseUnitDto1.logicalAmountUnits == ConverterService.GB

            ConverterResponseUnitDto responseUnitDto2 = response[2]
            assert responseUnitDto2.index == 2
            assert responseUnitDto2.sowItemId == 286300
            assert responseUnitDto2.targetLogicalAmount == 100
            assert responseUnitDto2.logicalAmountUnits == 'M mappable reads'

            ConverterResponseUnitDto responseUnitDto3 = response[3]
            assert responseUnitDto3.index == 3
            assert responseUnitDto3.sowItemId == 286185
            assert responseUnitDto3.targetLogicalAmount == 100
            assert responseUnitDto3.logicalAmountUnits == 'M reads'
    }
    @Test
    void "test convertUnitsToGb sow items"() {
        when:
            ConverterResponseUnitDto[] response = tlaConverterService.convertUnitsToGb(sowItems as ConverterRequestUnitDto[])
        then:
        assert response
        assert response.size() == 4

        ConverterResponseUnitDto responseUnitDto = response[0]
        assert responseUnitDto.libraryName == 'CWAXB'
        assert responseUnitDto.sowItemId == 284967
        assert responseUnitDto.targetLogicalAmount == 200
        assert responseUnitDto.logicalAmountUnits == 'X'
        assert responseUnitDto.fromLogicalAmount == 200
        assert responseUnitDto.fromLogicalAmountUnits == 'X'
        assert responseUnitDto.toLogicalAmount == 0.999999900
        assert responseUnitDto.toLogicalAmountUnits == ConverterService.GB

        ConverterResponseUnitDto responseUnitDto1 = response[1]
        assert responseUnitDto1.sowItemId == 286187
        assert responseUnitDto1.targetLogicalAmount == 6
        assert responseUnitDto1.logicalAmountUnits == ConverterService.GB
        assert responseUnitDto1.fromLogicalAmount == 6
        assert responseUnitDto1.fromLogicalAmountUnits == ConverterService.GB
        assert responseUnitDto1.toLogicalAmountUnits == ConverterService.GB
        assert responseUnitDto1.toLogicalAmount == 6

        ConverterResponseUnitDto responseUnitDto2 = response[2]
        assert responseUnitDto2.sowItemId == 286300
        assert responseUnitDto2.targetLogicalAmount == 100
        assert responseUnitDto2.logicalAmountUnits == 'M mappable reads'
        assert responseUnitDto2.fromLogicalAmount == 100
        assert responseUnitDto2.fromLogicalAmountUnits == 'M mappable reads'
        assert responseUnitDto2.toLogicalAmount == 15.000000000
        assert responseUnitDto2.toLogicalAmountUnits == ConverterService.GB

        ConverterResponseUnitDto responseUnitDto3 = response[3]
        assert responseUnitDto3.sowItemId == 286185
        assert responseUnitDto3.targetLogicalAmount == 100
        assert responseUnitDto3.logicalAmountUnits == 'M reads'
        assert responseUnitDto3.fromLogicalAmount == 100
        assert responseUnitDto3.fromLogicalAmountUnits == 'M reads'
        assert responseUnitDto3.toLogicalAmount == 15.000000000
        assert responseUnitDto3.toLogicalAmountUnits == ConverterService.GB

        when:
            // no units in GB
            response = tlaConverterService.convertUnitsToGb(
                    [sowItems[0], sowItems[2], sowItems[3]]  as ConverterRequestUnitDto[]
            )
        then:
        assert response
        assert response.size() == 3

            ConverterResponseUnitDto unitDto = response[0]
            assert unitDto.sowItemId == 284967
            assert unitDto.targetLogicalAmount == 200
            assert unitDto.logicalAmountUnits == 'X'
            assert unitDto.fromLogicalAmount == 200
            assert unitDto.fromLogicalAmountUnits == 'X'
            assert unitDto.toLogicalAmount == 0.999999900
            assert unitDto.toLogicalAmountUnits == ConverterService.GB

            ConverterResponseUnitDto unitDto1 = response[1]
            assert unitDto1.sowItemId == 286300
            assert unitDto1.targetLogicalAmount == 100
            assert unitDto1.logicalAmountUnits == 'M mappable reads'
            assert unitDto1.fromLogicalAmount == 100
            assert unitDto1.fromLogicalAmountUnits == 'M mappable reads'
            assert unitDto1.toLogicalAmount == 15.0
            assert unitDto1.toLogicalAmountUnits == ConverterService.GB

            ConverterResponseUnitDto unitDto2 = response[2]
            assert unitDto2.sowItemId == 286185
            assert unitDto2.targetLogicalAmount == 100
            assert unitDto2.logicalAmountUnits == 'M reads'
            assert unitDto2.fromLogicalAmount == 100
            assert unitDto2.fromLogicalAmountUnits == 'M reads'
            assert unitDto2.toLogicalAmount == 15.0
            assert unitDto2.toLogicalAmountUnits == ConverterService.GB
        when:
            // only units in GB
            response = tlaConverterService.convertUnitsToGb(
                    [sowItems[1]]  as ConverterRequestUnitDto[]
            )
        then:
        assert response
        assert response.size() == 1

        ConverterResponseUnitDto dto = response[0]
        assert dto.sowItemId == 286187
        assert dto.targetLogicalAmount == 6
        assert dto.logicalAmountUnits == ConverterService.GB
        assert dto.fromLogicalAmount == 6
        assert dto.fromLogicalAmountUnits == ConverterService.GB
        assert dto.toLogicalAmountUnits == ConverterService.GB
        assert dto.toLogicalAmount == 6
    }
}
