package gov.doe.jgi

import gov.doe.jgi.utils.FailureModesService
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class FailureModesServiceSpec {
    static final Logger log = LoggerFactory.getLogger(FailureModesServiceSpec.class)

    @Autowired
    FailureModesService failureModesService

    @Test
    void "fetchActiveFailureModes"() {
        given:
            String processType = ProcessTypeTest.SM_SAMPLE_QC.value
        when:
            List<String> failureModes = failureModesService.fetchActiveFailureModes(processType)
            log.info "failureModes $failureModes"
        then:
            assert failureModes.size() == 10
        when:
            processType = ProcessTypeTest.LC_LIBRARY_CREATION.value
            failureModes = failureModesService.fetchActiveFailureModes(processType)
        then:
            assert failureModes.size() == 8
            assert 'Instrument Error' in failureModes
            assert 'Operator Error' in failureModes
            assert 'Abandoned Work' in failureModes
    }
}
