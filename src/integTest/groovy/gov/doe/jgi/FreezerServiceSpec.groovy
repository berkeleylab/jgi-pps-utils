package gov.doe.jgi

import gov.doe.jgi.freezer.FreezerContainer
import gov.doe.jgi.freezer.FreezerDto
import gov.doe.jgi.freezer.FreezerService
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class FreezerServiceSpec {
    static final Logger logger = LoggerFactory.getLogger(FreezerService.class)
    @Autowired
    NodeManager nodeManager
    @Autowired
    FreezerService freezerService

    @BeforeEach
    void setup() {
        nodeManager = nodeManager.isClosed ? new NodeManager(nodeManager.nodeConfig) : nodeManager
    }
    @AfterEach
    void cleanup() {
        nodeManager.close()
    }

    @Test
    void "test freezerLookupInputAnalytes"() {
        setup:
            def processId = '122-886806'//production
            ProcessNode processNode = nodeManager.getProcessNode(processId)
            List<String> barcodes = processNode.inputAnalytes*.containerNode.unique()*.name
        when:
            List<FreezerDto> freezerLocations = freezerService.getContainerFreezerLocations(barcodes)
            List<FreezerContainer> freezerDtoNoDuplicates = freezerService.freezerLookupInputAnalytes(processNode)
        then:
            logger.debug barcodes.collect {
                FreezerDto freezerDto = freezerLocations.find{ dto -> dto.barcode == it }
                freezerDto.barcode + ',' + freezerDto.freezerLocation
            }.join('\n')

            freezerDtoNoDuplicates.each{ FreezerContainer freezerContainer ->
                String barcode = freezerContainer.barcode
                List<FreezerDto> freezerDtos = freezerLocations.findAll{it.barcode == barcode}
                assert freezerDtos.size() == 1
                assert freezerContainer.location == "${freezerDtos[0].freezerLocation}"
            }
    }
}
