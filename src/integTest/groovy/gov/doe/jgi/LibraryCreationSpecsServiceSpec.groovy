package gov.doe.jgi

import gov.doe.jgi.config.Organization
import gov.doe.jgi.model.LibraryCreationQueueCv
import gov.doe.jgi.model.LibraryCreationSpecsCv
import gov.doe.jgi.utils.LibraryCreationSpecsService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class LibraryCreationSpecsServiceSpec {
    @Autowired
    LibraryCreationSpecsService libraryCreationSpecsService
    @Test
    void "test getLibraryCreationSpecs JGI" () {
        when:
            List<LibraryCreationSpecsCv> lcSpecs = libraryCreationSpecsService.getLibraryCreationSpecs(Organization.JGI)
        then:
            assert lcSpecs
            lcSpecs.each { LibraryCreationSpecsCv lcs ->
                lcs.libraryCreationQueues.findAll { it.active}.each {
                    assert it.isPlateQueue || it.isTubeQueue
                }
                assert lcSpecs*.tubeLibraryCreationQueue()
                assert lcSpecs*.plateLibraryCreationQueue()
            }
    }
    @Test
    void "test getLibraryCreationSpecs not JGI" () {
        when:
            List<LibraryCreationSpecsCv> lcSpecs = libraryCreationSpecsService.getLibraryCreationSpecs(Organization.HUDSON_ALPHA)
        then:
            assert lcSpecs
            lcSpecs.each { LibraryCreationSpecsCv lcs ->
                lcs.libraryCreationQueues.findAll { it.active}.each {
                    assert it.isPlateQueue || it.isTubeQueue
                }
                assert lcSpecs*.tubeLibraryCreationQueue()
                assert lcSpecs*.plateLibraryCreationQueue()
            }
    }
    @Test
    void "test getLibraryCreationSpecs by id" () {
        setup:
            BigInteger lcSpecsId = 1
        when:
            LibraryCreationSpecsCv lcSpecs = libraryCreationSpecsService.getLibraryCreationSpecs(lcSpecsId, Organization.JGI)
        then:
            //check LC specs
            assert lcSpecs
            assert lcSpecs.libraryCreationSpecs == "Illumina Bisulphite-Seq"
            assert lcSpecs.active
            assert lcSpecs.libraryCreationQueues.size() == 2
            assert [1, 74].each{it in lcSpecs.libraryCreationQueues.collect { it.id }}
        when:
            //check LC Queues corresponding to Specs
            LibraryCreationQueueCv plateQueue = lcSpecs.plateLibraryCreationQueue()
            LibraryCreationQueueCv tubeQueue = lcSpecs.tubeLibraryCreationQueue()
        then:
            assert tubeQueue.libraryCreationQueue == "Illumina Bisulphite-Seq, Tubes"
            assert tubeQueue.id == 1
            assert plateQueue.libraryCreationQueue == "Illumina Bisulphite-Seq, Plates"
            assert plateQueue.id == 74
        when:
            BigInteger id = 91
            lcSpecs = libraryCreationSpecsService.getLibraryCreationSpecs(id, Organization.HUDSON_ALPHA)
        then:
            //check LC specs
            assert lcSpecs
            assert lcSpecs.libraryCreationSpecs == "Illumina, HiC Standard"
            assert !lcSpecs.active
            assert lcSpecs.libraryCreationQueues
            assert lcSpecs.plateLibraryCreationQueue() == lcSpecs.tubeLibraryCreationQueue()
    }
    @Test
    void "test getLibraryCreationSpecs by name" () {
        setup:
            String lcSpecsName = "Illumina Eukaryote smRNA"
        when:
            LibraryCreationSpecsCv lcSpecs = libraryCreationSpecsService.getLibraryCreationSpecs(lcSpecsName, Organization.JGI)
        then:
            //check LC specs
            assert lcSpecs
            assert lcSpecs.id == 2
            assert lcSpecs.active
            assert lcSpecs.libraryCreationQueues.size() == 2
            assert [2, 64].each{ it in lcSpecs.libraryCreationQueues.collect { it.id }}
        when:
            //check LC Queues corresponding to Specs
            LibraryCreationQueueCv plateQueue = lcSpecs.plateLibraryCreationQueue()
            LibraryCreationQueueCv tubeQueue = lcSpecs.tubeLibraryCreationQueue()
        then:
            assert tubeQueue.libraryCreationQueue == "Illumina Eukaryote smRNA, Tubes"
            assert tubeQueue.id == 2
            assert plateQueue.libraryCreationQueue == "Illumina Eukaryote smRNA, Plates"
            assert plateQueue.id == 64
        when:
            lcSpecs = libraryCreationSpecsService.getLibraryCreationSpecs("Illumina, HiC nuc prep", Organization.HUDSON_ALPHA)
        then:
            //check LC specs
            assert lcSpecs
            assert lcSpecs.id == 92
            assert !lcSpecs.active
            assert lcSpecs.libraryCreationQueues
            assert lcSpecs.plateLibraryCreationQueue() == lcSpecs.tubeLibraryCreationQueue()
    }
    @Test
    void "test getLibraryCreationQueues" () {
        when:
            List<LibraryCreationQueueCv> lcQueues = libraryCreationSpecsService.getLibraryCreationQueues(Organization.JGI)
        then:
            assert lcQueues
            lcQueues.findAll { it.active }.each {
                assert it.isPlateQueue || it.isTubeQueue
                assert it.libraryCreationSpecs
                assert it in [it.libraryCreationSpecs.tubeLibraryCreationQueue(), it.libraryCreationSpecs.plateLibraryCreationQueue()]
            }
        when:
            lcQueues = libraryCreationSpecsService.getLibraryCreationQueues(Organization.HUDSON_ALPHA)
        then:
            assert lcQueues.size()
    }
    @Test
    void "test getLibraryCreationQueues by id" () {
        setup:
            BigInteger lcSpecsId = 1
        when:
            LibraryCreationQueueCv lcQueue = libraryCreationSpecsService.getLibraryCreationQueue(lcSpecsId, Organization.JGI)
        then:
            assert lcQueue
            assert lcQueue.libraryCreationSpecs.libraryCreationSpecs == "Illumina Bisulphite-Seq"
            assert lcQueue.active
            assert lcQueue.id == 1
            assert lcQueue.libraryCreationQueue == "Illumina Bisulphite-Seq, Tubes"
    }
    @Test
    void "test getLibraryCreationQueues by name" () {
        setup:
            String specsName = "Illumina Eukaryote smRNA"
        when:
            LibraryCreationQueueCv lcQueue = libraryCreationSpecsService.getLibraryCreationQueue("$specsName, Tubes", Organization.JGI)
        then:
            assert lcQueue
            assert lcQueue.id == 2
            assert lcQueue.active
            assert lcQueue.libraryCreationSpecs.libraryCreationSpecs == specsName
            assert lcQueue.libraryCreationQueue == "$specsName, Tubes"
    }
}
