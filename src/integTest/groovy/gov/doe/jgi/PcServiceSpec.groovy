package gov.doe.jgi

import gov.doe.jgi.config.Organization
import gov.doe.jgi.cv.PlatformTypeCv
import gov.doe.jgi.cv.SequencerModelTypeCv
import gov.doe.jgi.domain.*
import gov.doe.jgi.mapper.RunModeCvMapper
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.utils.ClarityDataService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class PcServiceSpec {
    static final Logger log = LoggerFactory.getLogger(PcServiceSpec.class)

    @Autowired
    ClarityDataService clarityDataService

    @Test
    void "retrieve FinalDelivProduct by id"() {
        given:
        Serializable id = 7
        when:
        FinalDelivProduct entity = FinalDelivProduct.read(id)
        FinalDelivProduct expectedEntity = new FinalDelivProduct(
                id:id,
                finalDelivProductName: 'Plant/Other Eukaryote Annotated Standard Draft+',
                active: false
        )
        then:
        entity
        log.info("$entity")
        assert entity == expectedEntity
    }

    @Test
    void "retrieve LogicalAmountUnits by id"() {
        given:
        Serializable id = 7
        when:
        LogicalAmountUnits entity = LogicalAmountUnits.read(id)
        LogicalAmountUnits expectedEntity = new LogicalAmountUnits(
                id:id,
                logicalAmountUnits: 'M reads_CCS',
                active: true
        )
        then:
        entity
        log.info("$entity")
        assert entity == expectedEntity
    }


    @Test
    void "findByLogicalAmountUnits"() {
        given:
        String s = 'M reads_CCS'
        when:
        LogicalAmountUnits entity = LogicalAmountUnits.findByLogicalAmountUnits(s)
        LogicalAmountUnits expectedEntity = new LogicalAmountUnits(
                id:7,
                logicalAmountUnits: s,
                active: true
        )
        then:
        entity
        log.info("$entity")
        assert entity == expectedEntity
    }
}
