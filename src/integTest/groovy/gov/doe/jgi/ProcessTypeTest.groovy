package gov.doe.jgi

enum ProcessTypeTest {
    SM_SAMPLE_QC('SM Sample QC'),
    SM_SOW_ITEM_QC('SM SOW Item QC'),
    AC_ALIQUOT_CREATION('AC Sample Aliquot Creation'),
    LC_LIBRARY_CREATION('LC Library Creation'),
    LQ_LIBRARY_QPCR('LQ Library qPCR'),
    LP_POOL_CREATION('LP Pool Creation')

    final String value
    ProcessTypeTest(String value) {
        this.value = value
    }
    String toString() {
        value
    }
}