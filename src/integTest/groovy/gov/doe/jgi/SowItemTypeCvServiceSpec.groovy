package gov.doe.jgi

import gov.doe.jgi.cv.SowItemTypeCv
import gov.doe.jgi.domain.SowItemType
import gov.doe.jgi.utils.PcService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class SowItemTypeCvSpec {
    @Autowired
    PcService pcService

    @Test
    void "test fetchActiveSowItemTypes"() {
        when:
            Map<Integer, SowItemType> sowItemTypes = pcService.fetchSowItemTypes()
        then:
            assert sowItemTypes.size() >= SowItemTypeCv.values().size()
            SowItemTypeCv.values().each{SowItemTypeCv typeEnum ->
            SowItemType typeCv = sowItemTypes[typeEnum.id]
            assert typeCv
            assert typeCv.sowItemType == typeEnum.value
            assert typeCv.active
        }
    }
}
