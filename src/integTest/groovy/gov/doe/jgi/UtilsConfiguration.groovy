package gov.doe.jgi

import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.utils.ClarityDataService
import gov.doe.jgi.utils.LibraryCreationSpecsService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

@Configuration
@ComponentScan(["gov.doe.jgi"])
class UtilsConfiguration {
    static final Logger logger = LoggerFactory.getLogger(UtilsConfiguration.class)

    @Autowired
    Environment env

    @Bean
    ClarityDataService clarityDataService() {
        new ClarityDataService()
    }

    @Autowired
    ClaritySettings clarity

    @Bean
    NodeManager nodeManager() {
        new NodeManager(clarity.toNodeConfig())
    }
    @Value("\${wsAccessUrl}")
    private String wsAccessUrl

    @Bean
    WebClient wsAccessClient() {
        return WebClient.builder()
                .codecs({configurer -> configurer.defaultCodecs().maxInMemorySize(1024 * 1000 * 100)})
                .baseUrl(wsAccessUrl)
                .filters(exchangeFilterFunctions -> {
                    exchangeFilterFunctions.add(logRequest())
                })
                .build()
    }

    static ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            logger.info("Request: {} {}", clientRequest.method(), clientRequest.url())
            clientRequest.headers().forEach({name, values -> values.forEach({value -> logger.info("$name $value")})})
            return Mono.just(clientRequest)
        })
    }

    @Bean
    LibraryCreationSpecsService libraryCreationSpecsService() {
        return new LibraryCreationSpecsService()
    }
/*
    @Value("\${contactsUrl}")
    private String contactsUrl

    @Bean
    RESTClient contactsClient() {
        def client = new RESTClient(contactsUrl)
        client
    }

    @Value("\${collaboratorsUrl}")
    private String collaboratorsUrl

    @Bean
    @Qualifier("collaboratorsClient")
    WebClient collaboratorsClient() {
        // mutate() will *copy* the builder state and create a new one out of it
        return wsAccessClient().mutate()
                .baseUrl(collaboratorsUrl)
                .build()
    }*/
}
