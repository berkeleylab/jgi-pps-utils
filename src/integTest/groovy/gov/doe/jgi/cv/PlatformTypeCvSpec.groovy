package gov.doe.jgi.cv

import gov.doe.jgi.UtilsConfiguration
import gov.doe.jgi.config.Organization
import gov.doe.jgi.domain.RunMode
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class PlatformTypeCvSpec {

    @Test
    void "test PlatformTypeCv"() {
        when:
            Collection<RunMode> runModes = RunMode.list()
            def activeJgiPlatforms = runModes
                    .findAll{it.active == 'Y' && it.organizationName == Organization.JGI.name}
                    .collect{it.platform}.unique()
        then:
            assert activeJgiPlatforms.size() > 0
            PlatformTypeCv.values().each{
                assert it.value in activeJgiPlatforms
            }
    }
}
