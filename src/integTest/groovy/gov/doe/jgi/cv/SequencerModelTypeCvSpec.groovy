package gov.doe.jgi.cv

import gov.doe.jgi.UtilsConfiguration
import gov.doe.jgi.config.Organization
import gov.doe.jgi.domain.RunMode
import gov.doe.jgi.mapper.RunModeCvMapper
import gov.doe.jgi.model.RunModeCv
import gov.doe.jgi.model.SequencerModelCv
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class SequencerModelTypeCvSpec {

    @Test
    void "test SequencerModelTypeCv"() {
        when:
        Collection<RunMode> runModes = RunMode.list()
        List<RunMode> activeJgiRunModes = runModes.findAll{it.active == 'Y' && it.organizationName == Organization.JGI.name}
        List<RunMode> hudsonAlphaRunModes = runModes.findAll{it.organizationName == Organization.HUDSON_ALPHA.name}
        def jgiSequencerModels = activeJgiRunModes.collect{it.sequencerModel}.unique()
        def jgiSequencerModelsFromInactiveRunModes = runModes.findAll{
            it.active == 'N' && it.organizationName == Organization.JGI.name
        }.collect{it.sequencerModel}.unique()
        def hudsonAlphaSequencerModels = hudsonAlphaRunModes.collect{it.sequencerModel}.unique()
        then:
        assert activeJgiRunModes.size() > 0
        //all hudson alpha run modes are inactive
        hudsonAlphaRunModes.each{
            assert it.active == 'N'
        }
        //cannot test: all active JGI run modes have active sequencer models, no model status info in
        // https://ws-access-dev.jgi.doe.gov/dw-pc-run-mode
        SequencerModelTypeCv.values().each{
            if (it.value in [SequencerModelTypeCv.X10.value])
                assert it.value in hudsonAlphaSequencerModels
            else if (it.value in [SequencerModelTypeCv.SEQUEL_II_E.value,
                                  SequencerModelTypeCv.SEQUEL_II.value,
                                  SequencerModelTypeCv.NEXTSEQ_HO.value,
                                  SequencerModelTypeCv.NEXTSEQ_MO.value
            ]) {
                // only inactive run modes are associated with 'Sequell II'
                assert it.value in jgiSequencerModelsFromInactiveRunModes
            }
            else
                assert it.value in jgiSequencerModels
        }
        assert SequencerModelTypeCv.novaSeqPrefix == 'NovaSeq '
        assert SequencerModelTypeCv.novaSeqXPrefix == 'NovaSeqX'
        assert SequencerModelTypeCv.sequelPrefix == 'Sequel'
        when:
        List<RunMode> sequelRunModes = runModes.findAll{it.sequencerModel.startsWith(SequencerModelCv.SEQUEL)}
        List<RunModeCv> sequelRunModeCvs = RunModeCvMapper.toRunModeCvList(sequelRunModes)
        List<SequencerModelCv> sequencerModelCvs = sequelRunModeCvs.collect{it.sequencerModel}
        then:
        assert sequencerModelCvs.size() > 0
        sequencerModelCvs.each{
            assert it.isSequel
            // 'Sequel II e' -> inactiveHudsonAlphaSequencerModels
            // 'Sequel' -> jgiSequencerModelsFromInactiveRunModes
            assert it.sequencerModel in ['Sequel', 'Sequel II', 'Sequel IIe']
            assert 'Sequel II e' != it.sequencerModel
        }
        when:
        List<SequencerModelCv> activeSequencerModelCvs = RunModeCvMapper.toRunModeCvList(activeJgiRunModes)
                .collect{it.sequencerModel}
        then:
        assert activeSequencerModelCvs.size() > 0
        activeSequencerModelCvs.each{
            String flowcellType = it.getFlowcellType()
            if (it.isIlluminaNovaSeqX)
                assert flowcellType in [ContainerTypes.FLOW_CELL_10B.value,
                                        ContainerTypes.FLOW_CELL_25B.value,
                                        ContainerTypes.FLOW_CELL_1_5B.value]
            else if (it.isIlluminaNovaSeq)
                assert flowcellType in [ContainerTypes.FLOW_CELL_S2.value,
                                        ContainerTypes.FLOW_CELL_S4.value,
                                        ContainerTypes.FLOW_CELL_SP.value]
            else
                assert it.flowcellType == ""
        }
    }
}
