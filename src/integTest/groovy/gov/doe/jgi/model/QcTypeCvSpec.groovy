package gov.doe.jgi.model

import com.fasterxml.jackson.databind.ObjectMapper
import gov.doe.jgi.UtilsConfiguration
import gov.doe.jgi.domain.QcType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class QcTypeCvSpec {
    def json = """
[
{
"qc-type-id": 0,
"qc-type": "None",
"active": true
},
{
"qc-type-id": 31,
"qc-type": "smRNA+Quantity+Quality+Purity+Other",
"active": false
},
{
"qc-type-id": 228,
"qc-type": "Quantity+manual Quantity+manual Quality+manual Purity",
"active": true
}
]
"""
    List<QcType> qcTypeDtoList
    @BeforeEach
    void setup() {
        qcTypeDtoList = new ObjectMapper().readValue(json, QcType[].class)
    }
    @Test
    void "test get"() {
        setup:
            Long id = 228
        when:
            QcTypeCv typeCv = QcTypeCv.get(id)
            QcType dto = qcTypeDtoList.find{it.id == id }
        then:
            check(dto, typeCv)
    }
    @Test
    void "test findByQcType"() {
        setup:
            String qcTypeName = 'smRNA+Quantity+Quality+Purity+Other'
        when:
            QcTypeCv typeCv = QcTypeCv.findByQcType(qcTypeName)
            QcType dto = qcTypeDtoList.find{it.qcType == qcTypeName }
        then:
            check(dto, typeCv)
    }
    @Test
    void "test getActiveQcTypes"() {
        setup:
            List<QcType> activeDtoList = qcTypeDtoList.findAll{it.active == 'Y' }
            QcType activeDto0 = activeDtoList[0]
            QcType activeDto1 = activeDtoList[1]
        when:
            List<QcTypeCv> activeQcTypes = QcTypeCv.activeQcTypes
        then:
            assert activeDtoList.size() == 2
            assert activeQcTypes.size()
            activeQcTypes.each{assert it.active}
            def activeQcType0 = activeQcTypes.find{it.id == activeDto0.id}
            check(activeDto0, activeQcType0)
            def activeQcType1 = activeQcTypes.find{it.id == activeDto1.id}
            check(activeDto1, activeQcType1)
    }

    static def check(QcType dto, QcTypeCv typeCv) {
        assert (dto.active == 'Y') == typeCv.active
        assert dto.id == typeCv.id
        assert dto.qcType == typeCv.qcType
        return true
    }
}
