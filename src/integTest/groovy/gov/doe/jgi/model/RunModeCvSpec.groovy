package gov.doe.jgi.model

import com.fasterxml.jackson.databind.ObjectMapper
import gov.doe.jgi.UtilsConfiguration
import gov.doe.jgi.config.Organization
import gov.doe.jgi.domain.RunMode
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [UtilsConfiguration.class])
class RunModeCvSpec {
    def json = """
[
{
"run-mode-id": 1,
"run-mode": "1R Short",
"read-total": 1,
"read-length-bp": 100,
"estimated-duration-days": 0,
"raw-data-output-gb-channel": null,
"raw-reads-m-channel": null,
"estimated-cost-per-channel-flo": null,
"estimated-cost-per-gb": null,
"estimated-cost-per-m-reads": null,
"clarity-lims-queue-name": null,
"sequencer-model-id": 1,
"sequencer-model": "HiSeq",
"platform-id": 1,
"platform": "Illumina",
"organization-name": "JGI",
"active": false
},
{
    "run-mode-id": 57,
    "run-mode": "Illumina NovaSeq S4 2 X 150",
    "read-total": 2,
    "read-length-bp": 150,
    "estimated-duration-days": null,
    "raw-data-output-gb-channel": null,
    "raw-reads-m-channel": null,
    "estimated-cost-per-channel-flo": null,
    "estimated-cost-per-gb": null,
    "estimated-cost-per-m-reads": null,
    "clarity-lims-queue-name": "SQ Sequencing Illumina NovaSeq",
    "sequencer-model-id": 18,
    "sequencer-model": "NovaSeq S4",
    "platform-id": 1,
    "platform": "Illumina",
    "organization-name": "JGI",
    "active": true
},
{
"run-mode-id": 61,
"run-mode": "Illumina NovaSeq 1 X 125",
"read-total": 1,
"read-length-bp": 125,
"estimated-duration-days": null,
"raw-data-output-gb-channel": null,
"raw-reads-m-channel": null,
"estimated-cost-per-channel-flo": null,
"estimated-cost-per-gb": null,
"estimated-cost-per-m-reads": null,
"clarity-lims-queue-name": "SQ Sequencing Illumina X10",
"sequencer-model-id": 14,
"sequencer-model": "NovaSeq",
"platform-id": 1,
"platform": "Illumina",
"organization-name": "Hudson Alpha",
"active": false
}
]
"""
    List<RunMode> runModeDtoList

    @BeforeEach
    void setup() {
        runModeDtoList = new ObjectMapper().readValue(json, RunMode[].class)
    }
    @Test
    void "test getAllRunModes"() {
        setup:
            def dtoIds = runModeDtoList.collect{it.id}
        when:
            List<RunModeCv> allRunModes = RunModeCv.allRunModes
            String[] orgNames = allRunModes.collect{it.organizationName }.unique()
            def activeList = allRunModes.sort{it.active}.collect{it.active }.unique()
        then:
            assert allRunModes.size() > 70
            assert orgNames.size() == 2
            Organization.values().each{
                assert it.name in orgNames
            }
            assert activeList == [Boolean.FALSE, Boolean.TRUE]
            dtoIds.each{
                RunModeCv runModeCv = allRunModes.find{RunModeCv cv -> cv.id == it }
                assert runModeCv
            }
    }
    @Test
    void "test findByRunMode"() {
        setup:
            RunMode dto = runModeDtoList[1]
        when:
            RunModeCv runModeCv = RunModeCv.findByRunMode(dto.runMode)
        then:
            assert dto.id == 57
            check(dto, runModeCv)
        when:
            dto = runModeDtoList[2]
            runModeCv = RunModeCv.findByRunMode(dto.runMode)
        then:
            assert dto.id == 61
            assert dto.organizationName == Organization.HUDSON_ALPHA.name
            assert !runModeCv
    }
    @Test
    void "test getActiveRunModes"() {
        setup:
            RunMode dto = runModeDtoList[1]
        when:
            List<RunModeCv> activeRunModeCvList = RunModeCv.activeRunModes
            RunModeCv activeRunModeCv = activeRunModeCvList.find{it.id == dto.id}
        then:
            assert dto.id == 57
            assert activeRunModeCvList.size()
            activeRunModeCvList.each{assert it.active }
            check(dto, activeRunModeCv)
    }
    @Test
    void "test unique"() {
        /*
        jgi-clarity-scripts problem -> with two identical udfs the code below is returning two objects
        def runModeCvs = process.inputAnalytes.collect{it.runModeCv}.unique()
         */
        setup:
            String udfRunMode = 'Illumina NovaSeq SP 2 X 150'
            RunModeCv runModeCv1 = getRunModeCv(udfRunMode)
            RunModeCv runModeCv2 = getRunModeCv(udfRunMode)
        when:
            def runModeCvs = [runModeCv1, runModeCv2].collect { it }.unique()
        then:
            assert runModeCvs.size() == 1
    }

    static RunModeCv getRunModeCv(String udfRunMode) {
        String runModeName = udfRunMode
        if (runModeName) {
            return RunModeCv.findByRunMode(runModeName)
        }
        return null
    }

    static def check(RunMode dto, RunModeCv runModeCv) {
        assert runModeCv.organizationName in Organization.values().collect {it.name}
        assert runModeCv.organizationName == dto.organizationName
        assert runModeCv.active == (dto.active == 'Y')
        assert runModeCv.id == dto.id
        assert runModeCv.runMode == dto.runMode
        assert runModeCv.sequencerModel.id == dto.sequencerModelId
        assert runModeCv.sequencerModel.sequencerModel == dto.sequencerModel
        assert runModeCv.sequencerModel.platformCv.platform == dto.platform
        assert runModeCv.sequencerModel.platformCv.id == dto.platformId
        assert runModeCv.clarityLimsQueueName == dto.workflowName
        assert runModeCv.readLengthBp == dto.readLengthBp
        assert runModeCv.readTotal == dto.readTotal
        return true
    }
}
