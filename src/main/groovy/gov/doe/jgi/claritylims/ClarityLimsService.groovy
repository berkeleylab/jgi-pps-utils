package gov.doe.jgi.claritylims

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.jdbc.core.BeanPropertyRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service

import static gov.doe.jgi.utils.ClarityDataService.CLARITY_LIMS

@Service
class ClarityLimsService {

    private final static def getInQueueProgressQuery() {
        return """
--- analytes are in queue and in progress
SELECT art.LUID,
  lw.WORKFLOWNAME
FROM ${CLARITY_LIMS}.STAGETRANSITION st,
  ${CLARITY_LIMS}.STAGE s,
  ${CLARITY_LIMS}.ARTIFACT art,
  ${CLARITY_LIMS}.PROTOCOLSTEP ps,
  ${CLARITY_LIMS}.PROCESSTYPE pt,
  ${CLARITY_LIMS}.WORKFLOWSECTION ws,
  ${CLARITY_LIMS}.LABWORKFLOW lw
WHERE art.ARTIFACTID = st.ARTIFACTID
AND st.STAGEID       = s.STAGEID
AND s.STEPID         = ps.STEPID
AND pt.TYPEID        = ps.PROCESSTYPEID
AND st.WORKFLOWRUNID > 0
AND ws.SECTIONID     = s.MEMBERSHIPID
AND lw.WORKFLOWID    = ws.WORKFLOWID
AND lw.WORKFLOWNAME IN (:workflowNames)
ORDER BY pt.DISPLAYNAME,
  art.NAME
"""
    }

    @Autowired
    @Qualifier("limsJdbcTemplate")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate

    List<ClarityQueueDto> getClarityQueueDtoList(List<String> workflowNames) {
        MapSqlParameterSource parameters = new MapSqlParameterSource("workflowNames", workflowNames)
        List<ClarityQueueDto> queueAnalytes = namedParameterJdbcTemplate.query(
                getInQueueProgressQuery(),
                parameters,
                new BeanPropertyRowMapper(ClarityQueueDto.class)
        )
        return queueAnalytes
    }
}