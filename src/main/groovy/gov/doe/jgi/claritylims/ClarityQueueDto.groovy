package gov.doe.jgi.claritylims

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
class ClarityQueueDto {
    String luid
    String workflowName
    //TODO get it from query
    String queueTime
}
