package gov.doe.jgi.config

/**
 * Created by lvishwas on 12/8/2015.
 */
enum Action {
    UNKNOWN(0),
    NO_ACTION(-1),
    REPEAT(-2),
    ESCALATE(-3),
    REMOVE(-4),
    COMPLETE(-5),
    ADD(-6),
    REQUEUE(-7),
    STORE(-8),
    REWORK(-9),
    STEPACTION(-10),
    REWORK_ACTION(-11),
    NO_LONGER_AVAILABLE(-12),
    COMPLETE_REPEAT(-13),
    COMPLETE_REPEAT_ACTION(-14)

    long actionId

    private Action(long actionId){
        this.actionId = actionId
    }
}
