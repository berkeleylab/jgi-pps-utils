package gov.doe.jgi.config

import gov.doe.jgi.pi.pps.clarity_node_manager.util.UdfAttachedTo
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UdfFieldTypes
import gov.doe.jgi.pi.pps.clarity_node_manager.util.UserDefinedField

/*
 * Created by dscott on 4/30/2014.
 *
 * http://frow.jgi-psf.org:8080/api/v2/configuration/udfs
 <cnf:udfs xmlns:cnf="http://genologics.com/ri/configuration">
 <udfconfig attach-to-category="" name="cocktail" attach-to-name="Analyte" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/1"/>
 <udfconfig attach-to-category="" name="comments" attach-to-name="Analyte" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/56"/>
 <udfconfig attach-to-category="ProcessType" name="Risk Level" attach-to-name="SM 100 Assign Bio Safety Level" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/4"/>
 <udfconfig attach-to-category="ProcessType" name="Sample Received" attach-to-name="SM 400 Sample Receipt" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/6"/>
 <udfconfig attach-to-category="ProcessType" name="Sample Received" attach-to-name="Unknown Type" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/7"/>
 <udfconfig attach-to-category="ProcessType" name="Sample Received" attach-to-name="SM 500 Sample QC" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/101"/>
 <udfconfig attach-to-category="ProcessType" name="Sample Received" attach-to-name="Unknown Type" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/102"/>
 <udfconfig attach-to-category="" name="JGI Barcode" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/51"/>
 <udfconfig attach-to-category="" name="Vendor Barcode" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/52"/>
 <udfconfig attach-to-category="" name="Sample Receipt Pass / Fail" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/53"/>
 <udfconfig attach-to-category="" name="Sample Receipt Dry Ice" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/54"/>
 <udfconfig attach-to-category="" name="Sample Receipt Comments" attach-to-name="Container" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/55"/>
 <udfconfig attach-to-category="" name="PMO Sample ID" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/2"/>
 <udfconfig attach-to-category="" name="Risk Level" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/3"/>
 <udfconfig attach-to-category="" name="Sample Receipt Comments" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/5"/>
 <udfconfig attach-to-category="" name="Long Comments" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/57"/>
 <udfconfig attach-to-category="" name="RNA / DNA" attach-to-name="Sample" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/103"/>
 </cnf:udfs>

 <cnf:field xmlns:cnf="http://genologics.com/ri/configuration" uri="http://frow.jgi-psf.org:8080/api/v2/configuration/udfs/103" type="String">
 <name>RNA / DNA</name>
 <attach-to-name>Sample</attach-to-name>
 <show-in-lablink>false</show-in-lablink>
 <allow-non-preset-values>false</allow-non-preset-values>
 <first-preset-is-default-value>false</first-preset-is-default-value>
 <show-in-tables>true</show-in-tables>
 <is-editable>false</is-editable>
 <is-deviation>false</is-deviation>
 <is-controlled-vocabulary>false</is-controlled-vocabulary>
 <preset>DNA</preset>
 <preset>RNA</preset>
 <is-required>false</is-required>
 <attach-to-category/>
 </cnf:field>
 */

enum ClarityUdf {

    RESEARCHER_CONTACT_ID('contact_id', UdfAttachedTo.RESEARCHER, UdfFieldTypes.NUMERIC),

    PROJECT_SEQUENCING_PROJECT_ID('PMO Sequencing Project ID', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SAMPLE_CONTACT('Sample Contact', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SAMPLE_CONTACT_ID('Sample Contact Id', UdfAttachedTo.PROJECT, UdfFieldTypes.NUMERIC),
    PROJECT_SEQUENCING_PROJECT_NAME('Sequencing Project Name', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SEQUENCING_PRODUCT_NAME('Sequencing Product Name', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SEQUENCING_PROJECT_PI('Sequencing Project PI', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_SEQUENCING_PROJECT_PI_CONTACT_ID('Sequencing Project PI Contact Id', UdfAttachedTo.PROJECT, UdfFieldTypes.NUMERIC),
    PROJECT_PROPOSAL_ID('Proposal Id', UdfAttachedTo.PROJECT, UdfFieldTypes.NUMERIC),
    PROJECT_SEQUENCING_PROJECT_MANAGER_ID('Sequencing Project Manager Id', UdfAttachedTo.PROJECT, UdfFieldTypes.NUMERIC),
    PROJECT_SEQUENCING_PROJECT_MANAGER('Sequencing Project Manager', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_MATERIAL_CATEGORY('Material Category', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_AUTO_SCHEDULE_SOW_ITEMS('AutoSchedule Sow Items', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),
    PROJECT_CREATED_BY('Created By', UdfAttachedTo.PROJECT, UdfFieldTypes.STRING),

    CONTAINER_LABEL('Label', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_FREEZER_LOCATION('Freezer Location', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_NORMALIZATION_NM('Normalization (nm)', UdfAttachedTo.CONTAINER, UdfFieldTypes.NUMERIC),
    CONTAINER_DILUTION('Dilution', UdfAttachedTo.CONTAINER, UdfFieldTypes.NUMERIC),
    CONTAINER_SOURCE_PLATE_LIMSID('Source Plate LIMSID', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_PROCESS_LIMSID('Process LIMSID', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_NORMALIZATION_LABEL('Normalization Label', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_DILUTION_LABEL('Dilution Label', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_NOTES('Notes', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_FINAL_ALIQUOT_VOLUME_UL('Final Aliquot Volume (uL)', UdfAttachedTo.CONTAINER, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    CONTAINER_FINAL_ALIQUOT_MASS_NG('Final Aliquot Mass (ng)', UdfAttachedTo.CONTAINER, UdfFieldTypes.NUMERIC, UdfUnits.NG),

    CONTAINER_LIBRARY_QC_FAILURE_MODE('Library QC Failure Mode', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_LIBRARY_QC_RESULT('Library QC Result', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_SAMPLE_QC_RESULT('Sample QC Result', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_LIBRARY_QPCR_RESULT('Library qPCR Result', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),
    CONTAINER_LAB_RESULT('Lab Result', UdfAttachedTo.CONTAINER, UdfFieldTypes.STRING),

    SAMPLE_LC_ATTEMPT('LC Attempt',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC),
    SAMPLE_LC_FAILED_ATTEMPT('LC Failed Attempt',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC),
    SAMPLE_DEGREE_OF_POOLING('Degree of Pooling', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_RUN_MODE('Run Mode', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_LIBRARY_CREATION_SPECS_ID('Library Creation Specs Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_MATERIAL_TYPE('Material Type', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_VOLUME_UL('Volume (uL)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_INITIAL_VOLUME_UL('Initial Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_GEL_DILUTE_VOLUME_UL('Gel Diluent Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_PURITY_VOLUME_UL('Purity Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_QUALITY_VOLUME_UL('Quality Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_QUANTITY_VOLUME_UL('Quantity Volume (ul)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_TARGET_ALIQUOT_MASS_NG('Target Aliquot Mass (ng)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.NG),
    SAMPLE_TARGET_TEMPLATE_SIZE_BP('Target Template Size (bp)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.BP),
    SAMPLE_NOTES('Notes', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_FORMAT('Format',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_COLLABORATOR_VOLUME_UL('Collaborator Volume (ul)',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC, UdfUnits.UL),
    SAMPLE_COLLABORATOR_CONCENTRATION_NGUL('Collaborator Concentration (ng/ul)',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC, UdfUnits.NG_PER_UL),
    SAMPLE_COLLABORATOR_SAMPLE_NAME('Collaborator Sample Name',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_ID('SOW Item ID', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_NUMBER('Sow Item Number', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_LIMSID('Sample LIMSID', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_LOGICAL_AMOUNT_UNITS('Logical Amount Units',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_SOW_ITEM_TYPE('Sow Item Type',UdfAttachedTo.SAMPLE,UdfFieldTypes.STRING),
    SAMPLE_TARGET_LOGICAL_AMOUNT('Target Logical Amount',UdfAttachedTo.SAMPLE,UdfFieldTypes.NUMERIC),
    SAMPLE_ACCOUNT_ID('Account ID',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_TARGET_INSERT_SIZE_KB('Target Insert Size (kb)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_LIBRARY_INDEX_SET('Library Index Set',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SCIENTIFIC_PROGRAM('Scientific Program',UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_STOP_AT_RECEIPT('Stop at Receipt',UdfAttachedTo.SAMPLE, UdfFieldTypes.BOOLEAN),
    SAMPLE_RECEIPT_NOTES('Sample Receipt Notes', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_RECEIPT_MISSING_DRY_ICE('Sample Receipt Missing Dry Ice', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_RECEIPT_DATE('Sample Receipt Date', UdfAttachedTo.SAMPLE, UdfFieldTypes.DATE),
    SAMPLE_RECEIPT_RESULT('Sample Receipt Result', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_QC_RESULT('Sample QC Result', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_QC_FAILURE_MODE('Sample QC Failure Mode', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_QC_RESULT('Sow Item QC Result', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SOW_QC_FAILURE_MODE('Sow Item QC Failure Mode', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CUMULATIVE_QC_TYPE_ID('Cumulative QC Type Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_CURRENT_QC_TYPE('Current QC Type', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_QC_TYPE_ID('QC Type Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_REQUIRED_QC_TYPE('Required QC Type', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_QC_DATE('Sample QC Date', UdfAttachedTo.SAMPLE, UdfFieldTypes.DATE),
    SAMPLE_QC_INSTRUMENT('Sample QC Instrument', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CONCENTRATION_NG_UL('Concentration (ng/uL)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC, UdfUnits.NG_PER_UL),
    SAMPLE_ABSORBANCE_260_230('Absorbance 260/230', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_ABSORBANCE_260_280('Absorbance 260/280', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_HMW_GDNA_Y_N('HMW gDNA Eval', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CALCULATED_HMW_GDNA('Calculated HMW gDNA Eval', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_CALCULATED_QC_RESULT('Calculated QC Result', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_QUALITY_SCORE('Quality Score', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_RRNA_RATIO('rRNA Ratio', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_QC_NOTES('Sample QC Notes', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_SM_INSTRUCTIONS('SM Instructions', UdfAttachedTo.SAMPLE, UdfFieldTypes.TEXT),
    SAMPLE_LC_INSTRUCTIONS('LC Instructions', UdfAttachedTo.SAMPLE, UdfFieldTypes.TEXT),
    SAMPLE_SQ_INSTRUCTIONS('SQ Instructions', UdfAttachedTo.SAMPLE, UdfFieldTypes.TEXT),
    SAMPLE_EXTERNAL('External', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_DEST_CONTAINER_NAME('Destination Container Name', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_DEST_CONTAINER_LOCATION('Destination Container Location', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_POOL_NUMBER('Pool Number', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_BATCH_ID('Batch Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_GROUP_NAME('Group Name', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_ISOTOPE_LABEL('Isotope Label', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_METABOLOMICS_SAMPLE_ID('Metabolomics Sample Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),
    SAMPLE_ISOTOPE_ENRICHMENT('Isotope Enrichment (at%)', UdfAttachedTo.SAMPLE, UdfFieldTypes.NUMERIC),
    SAMPLE_PC_WORKFLOW_ID('PC Workflow Id', UdfAttachedTo.SAMPLE, UdfFieldTypes.STRING),

    ANALYTE_AUTOMATION_NAME('Automation Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING), //@Deprecated
    ANALYTE_VOLUME_UL('Volume (uL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    ANALYTE_CONCENTRATION_NG_UL('Concentration (ng/uL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.NG_PER_UL),
    ANALYTE_ACTUAL_TEMPLATE_SIZE_BP('Actual Template Size (bp)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.BP),
    ANALYTE_ACTUAL_INSERT_SIZE_KB('Actual Insert Size (kb)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.KB),
    ANALYTE_LOWER_CUT_SIZE_KB('Lower Cut Size (kb)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.KB),
    ANALYTE_LIBRARY_MOLARITY_QC_PM('Library Molarity QC (pm)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.PM),
    ANALYTE_LIBRARY_MOLARITY_PM('Library Molarity (pm)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.PM),
    ANALYTE_INDEX_NAME('Index Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_INDEX_CONTAINER_BARCODE('Index Container Barcode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_NOTES('Notes', UdfAttachedTo.ANALYTE,UdfFieldTypes.STRING),
    ANALYTE_NUMBER_PCR_CYCLES("Number of PCR Cycles", UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_ACTUAL_LIBRARY_CREATION_QUEUE_ID('Actual Library Creation Queue Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_CREATION_QUEUE_ID('Library Creation Queue Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_CREATION_QUEUE('Library Creation Queue', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_CREATION_SPECS_ID('Library Creation Specs Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_CREATOR('Library Creator', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_QC_FAILURE_MODE('Library QC Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_QC_RESULT('Library QC Result', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_CALCULATED_FA_QC_RESULT('Calculated QC Result', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_RUN_MODE('Run Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_DEGREE_POOLING('Degree of Pooling', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_CONVERSION_FACTOR('Library Conversion Factor', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_PHIX_SPIKE_IN('PhiX Spike in %', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_FAILURE_MODE('Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_CLUSTER_GEN_FAILURE_MODE('Cluster Generation Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_SEQUENCING_FAILURE_MODE('Sequencing Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_QPCR_FAILURE_MODE('Library qPCR Failure Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LIBRARY_QPCR_RESULT('Library qPCR Result', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_LAB_RESULT('Lab Result', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_VOLUME_USED_UL('Volume Used (uL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    ANALYTE_SMRT_LINK_UUID('SMRT Link UUID', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_MOVIE_TIME_PER_CELL('Movie Time per SMRT Cell', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_ACQUISITION_TIME('Acquisition Time', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),  //@Deprecated
    ANALYTE_PACBIO_INSERT_SIZE('PacBio Insert Size', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_SEQUENCING_MODE('Sequencing Mode', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),  //@Deprecated
    ANALYTE_GENERATE_CCS_DATA('Generate CCS Data', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_SIZE_SELECTION('Size Selection', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_MAGBEAD_CLEANUP_DATE('MagBead Cleanup Date', UdfAttachedTo.ANALYTE, UdfFieldTypes.DATE),
    ANALYTE_PACBIO_MOLARITY_NM('PacBio Molarity (nm)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.NM),
    ANALYTE_FINAL_ALIQUOT_MASS_NG('Final Aliquot Mass (ng)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.NG),
    ANALYTE_FINAL_ALIQUOT_VOLUME_UL('Final Aliquot Volume (uL)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC, UdfUnits.UL),
    ANALYTE_EXTERNAL('External', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_COLLABORATOR_LIBRARY_NAME('Collaborator Library Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_COLLABORATOR_POOL_NAME('Collaborator Pool Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_PACBIO_LOADING_CONCENTRATION_NM('PacBio Loading Concentration (nM)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LIBRARY_PERCENTAGE_SOF('Library Percentage with SOF (%)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_LP_ACTUAL_WITH_SOF('Library Actual Percentage with SOF (%)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_KAPA_LOT_ID('Kapa Lot Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_KAPA_BATCH_ID('Kapa Batch Id', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_KAPA_TAG_NAME('Kapa Tag Name', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_KAPA_TAG_SEQUENCE('Kapa Tag Sequence', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),
    ANALYTE_KAPA_TAG_MASS_NG('Kapa Tag Mass (ng)', UdfAttachedTo.ANALYTE, UdfFieldTypes.NUMERIC),
    ANALYTE_REQUEUE('Requeue', UdfAttachedTo.ANALYTE, UdfFieldTypes.STRING),

    PROCESS_BARCODE_PRINTER("Printer", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_NORMALIZATION("Normalization", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DILUTION("Dilution", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),

    PROCESS_QC_TODO("QC TODO", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_FLOWCELL_BARCODE('Flowcell Barcode', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_BINDING_KIT_BOX_BARCODE("Binding Kit Box Barcode", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_POLYMERASE_KIT_BOX_BARCODE("Polymerase Kit Box Barcode", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SMRTBELL_TEMPLATE_PREP_KIT("SMRTbell Template Prep Kit", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_PACBIO_SMRTBELL_TEMPLATE_PREP_KIT("PacBio SMRTbell Template Prep Kit", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_PACBIO_SEQUENCER('PacBio Sequencer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SEQUEL_SEQUENCER('Sequel Sequencer', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DNTP_V3('dNTP v3', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),

    PROCESS_QUBIT_ASSAY_KIT('Qubit Assay Kit', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_SMRT_LINK_UUID('SMRT Link UUID', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),  //@Deprecated

    PROCESS_DNA_INTERNAL_CONTROL_COMPLEX_LOT('DNA Internal Control Complex Lot', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_DNA_SEQUENCING_REAGENT_KIT('DNA Sequencing Reagent Kit', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_KAPA_SPIKE_IN_PLATE_BARCODE("Kapa Spike-in Plate Barcode", UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_MIN_DNA_CONCENTRATION_NG_UL('Minimum DNA Concentration (ng/uL)', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC),
    PROCESS_REPLICATES_FAILURE_PERCENT_ALLOWED('Replicates Failure Percent Allowed', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC),
    PROCESS_CONTROLS_FAILURE_PERCENT_ALLOWED('Controls Failure Percent Allowed', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC),
    PROCESS_MINIMUM_ISOTOPE_ENRICHMENT('Minimum Isotope Enrichment (at%)', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC),
    PROCESS_MORE_DAP_EXPERIMENTS('More DAP Seq Experiments?', UdfAttachedTo.PROCESS, UdfFieldTypes.STRING),
    PROCESS_PARTIAL_PLATE_MINIMUM('Partial Plate Minimum', UdfAttachedTo.PROCESS, UdfFieldTypes.NUMERIC)

    final String value
    final UdfAttachedTo udfAttachedTo
    final UdfFieldTypes udfFieldType
    final UdfUnits udfUnits
    final UserDefinedField udf

    final static String DEFAULT_UDF_VALUE = 'Unknown'

    private static final Map<UdfAttachedTo, Map<String, ClarityUdf>> converter = new HashMap<UdfAttachedTo, Map<String, ClarityUdf>>()
    private static boolean converterInitialized = false
    private static void initializeConverter() {
        if (!converterInitialized) {
            synchronized(converter) {
                converter.clear()
                if (!converterInitialized) {
                    for (ClarityUdf udfs : values()) {
                        UdfAttachedTo owner = udfs.udfAttachedTo
                        Map<String, ClarityUdf> nameMap = converter[owner]
                        if (!nameMap) {
                            nameMap = [:]
                            converter[owner] = nameMap
                        }
                        if (nameMap.containsKey(udfs.value)) {
                            throw new RuntimeException("duplicate UDF entry for name ${udfs.value} and owner ${owner}")
                        }
                        nameMap[udfs.value] = udfs
                    }
                    converterInitialized = true
                }
            }
        }
    }

    static ClarityUdf toEnum(String udfName, UdfAttachedTo owner = null) {
        ClarityUdf foundUdfs = null
        initializeConverter()
        if (owner) {
            Map<String, ClarityUdf> nameMap = converter[owner]
            if (nameMap) {
                foundUdfs = nameMap[udfName]
            }
        } else {
            for (Map<String, ClarityUdf> nameMap : converter.values()) {
                if (nameMap) {
                    foundUdfs = nameMap[udfName]
                }
                if (foundUdfs) {
                    break
                }
            }
        }
        return foundUdfs
    }

    private ClarityUdf(String value, UdfAttachedTo udfAttachedTo, UdfFieldTypes udfFieldType, UdfUnits udfUnits = null) {
        this.value = value
        this.udfAttachedTo = udfAttachedTo
        this.udfFieldType = udfFieldType
        this.udfUnits = udfUnits
        this.udf = new UserDefinedField(name:value,attachedTo:udfAttachedTo,type:udfFieldType,units:udfUnits, attachToName: udfAttachedTo.value)
    }

    String toString() {return value}
}