package gov.doe.jgi.config

import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode
import gov.doe.jgi.pi.pps.clarity_node_manager.util.OnDemandCache
import gov.doe.jgi.utils.BeanUtil

enum ClarityWorkflow {
    SUPPLY_CHAIN_MANAGEMENT('Supply Chain Management'),
    RECEIVE_SAMPLES('Receive Samples'),
	SAMPLE_RECEIPT('Sample Receipt Workflow'),
    SAMPLE_QC_DNA('Sample QC_DNA'),
    SAMPLE_QC_RNA('Sample QC_RNA'),
    SOW_ITEM_QC('SOW Item QC'),
	SAMPLE_QC_METABOLOMICS('SM Sample QC Metabolomics'),
	SAMPLE_FRACTIONATION('SM Sample Fractionation'),
    ALIQUOT_CREATION_DNA('Sample Aliquot DNA'),
    ALIQUOT_CREATION_RNA('Sample Aliquot RNA'),

	LC_NOT_JGI('LC Not JGI'),

    SIZE_SELECTION('Size Selection'),
    PLATE_TO_TUBE_TRANSFER('Plate To Tube Transfer'),
	LIBRARY_QUANTIFICATION('Library Quantification Workflow'),
	POOL_CREATION('Pool Creation Workflow'),

	PACBIO_SEQUEL_LIBRARY_ANNEALING('PacBio Sequel Library Annealing'),
	PACBIO_SEQUEL_LIBRARY_BINDING('PacBio Sequel Library Binding'),
	PACBIO_SEQUEL_II_SEQUENCING_PREP('PacBio Sequel II Sequencing Prep'),
	PACBIO_SEQUEL_SEQUENCING_COMPLETE('PacBio Sequel Sequencing Complete'),
	PACBIO_SEQUEL_MAGBEAD_CLEANUP('PacBio Sequel MagBead Cleanup'),

	PACBIO_REVIO_LIBRARY_ANNEALING('PacBio Revio Library Annealing'),
	PACBIO_REVIO_LIBRARY_BINDING('PacBio Revio Library Binding'),
	PACBIO_REVIO_SEQUENCING_PREP('PacBio Revio Sequencing Prep'),
	PACBIO_REVIO_SEQUENCING_COMPLETE('PacBio Revio Sequencing Complete'),

	SEQUENCING_ILLUMINA_MISEQ('SQ Sequencing Illumina MiSeq'),
    SEQUENCING_ILLUMINA_NOVASEQ('SQ Sequencing Illumina NovaSeq'),
	SEQUENCING_ILLUMINA_NEXTSEQ('SQ Sequencing Illumina NextSeq'),

	ABANDON_QUEUE('Abandon Queue'),
	ABANDON_WORK('Abandon Work'),
	ON_HOLD_WORKFLOW('On Hold'),
	RELEASE_HOLD_WORKFLOW('Release from Hold'),
	NEEDS_ATTENTION_WORKFLOW('Needs Attention'),
	RELEASE_NEEDS_ATTENTION_WORKFLOW('Release from Needs Attention'),

	REQUEUE_LIBRARY_CREATION('Requeue for Library Creation'),
	REQUEUE_LIBRARY_ANNEALING('Requeue for Library Annealing'),

    REQUEUE_LIBRARY_BINDING('Requeue for Library Binding'),
	AUTOMATIC_REQUEUE_LIBRARY_CREATION('Automatic Requeue for Library Creation'),
	AUTOMATIC_REQUEUE_SEQUENCING('Automatic Requeue for Sequencing'),
	AUTOMATIC_REQUEUE_SEQUENCE_ANALYSIS('Automatic Requeue for Sequencing Analysis'),
	AUTOMATIC_REQUEUE_QPCR('Automatic Requeue for qPCR'),
	AUTOMATIC_POOL_CREATION_WORKFLOW('Automatic Pool Creation Workflow'),
	AUTOMATIC_ROUTE_TO_WORKFLOW('Automatic Route to Workflow'),
	AUTOMATIC_SOW_ITEM_QC_WORKFLOW('Automatic SOW Item QC'),
	AUTOMATIC_CREATE_CONTAINERS('Automatic Create Containers'),

	SHIP_OFFSITE('Ship Off-site'),
	TRASH('Trash'),
	ON_SITE('On Site'),
	ADJUST_MASS('Adjust Mass'),
	ADJUST_MOLARITY('Adjust Molarity'),
	ADJUST_VOLUME('Adjust Volume'),

	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_LIBRARY_CREATION('Automatic Sequence QC Requeue for Library Creation'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_PACBIO_LIBRARY_ANNEALING('Automatic Sequence QC Requeue for PacBio Library Annealing'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_PACBIO_LIBRARY_BINDING('Automatic Sequence QC Requeue for PacBio Library Binding'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_POOL_CREATION('Automatic Sequence QC Requeue for Pool Creation'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_QPCR('Automatic Sequence QC Requeue for qPCR'),
	AUTOMATIC_SEQUENCE_QC_REQUEUE_FOR_SEQUENCING('Automatic Sequence QC Requeue for Sequencing'),
	AUTOMATIC_SOWITEM_REQUEUE_FOR_POOL_CREATION('Automatic SowItem Requeue for Pool Creation'),
	AUTOMATIC_SOWITEM_REQUEUE_FOR_SEQUENCING('Automatic SowItem Requeue for Sequencing'),

	ONBOARD_SEQUENCING_FILES('Onboard Sequencing Files'),
	PLACE_IN_NEEDS_ATTENTION('Place in Needs Attention'),
	PLACE_ON_HOLD('Place on Hold'),
	PRINT_LABELS('Print Labels'),
	REQUEUE_FOR_QC('Requeue for QC'),
	SEQUENCE_QC_REQUEUE_FOR_LIBRARY_CREATION('Sequence QC Requeue for Library Creation'),
	SEQUENCE_QC_REQUEUE_FOR_QPCR('Sequence QC Requeue for qPCR'),
    SEQUENCE_QC_REQUEUE_FOR_SEQUENCING('Sequence QC Requeue for Sequencing')

    final String value

	private final OnDemandCache cachedStages = new OnDemandCache<>()
	List<Stage> getStages() {
		cachedStages.fetch {
			List<Stage> stageList = []
			Stage.values().each { Stage stage ->
				if (stage.workflow.is(this)) {
					stageList << stage
				}
			}
			stageList
		} as List<Stage>
	}

    ClarityWorkflow(String value) {
		this.value = value
	}

	WorkflowNode getWorkflowNode() {
		BeanUtil.nodeManager.getWorkflowNodeByName(value)
	}

	String getUri() {
		workflowNode?.workflowUri
	}

    Integer getId() {
		workflowNode?.workflowId
    }

	static ClarityWorkflow toEnum(value) {
		return values().find { it.value == value }
	}

	String toString() {
		value
	}

	Boolean getIsPacBioSequel() {
		name().startsWith('PACBIO_SEQUEL')
	}

	Boolean getIsPacBioRevio() {
		name().startsWith('PACBIO_REVIO')
	}

	Boolean getIsPacBio() {
		name().startsWith('PACBIO')
	}

	static List<ClarityWorkflow> sampleAliquotCreationQueues() {
		[ALIQUOT_CREATION_DNA, ALIQUOT_CREATION_RNA]
	}
}
