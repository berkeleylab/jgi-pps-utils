package gov.doe.jgi.config

enum Organization {
    JGI("JGI"),
    HUDSON_ALPHA("Hudson Alpha")

    String name

    Organization(String orgName) {
        name = orgName
    }

    static Organization toEnum(String orgName) {
        return values().find { it.name == orgName }
    }

    String toString() {
        return name
    }
}