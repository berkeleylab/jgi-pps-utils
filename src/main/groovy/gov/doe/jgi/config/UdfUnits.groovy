package gov.doe.jgi.config

enum UdfUnits {
    ML('mL'),
    UL('uL'),
    NG_PER_UL('ng/uL'),
	PMOL_PER_L('pmol/L'),
	NG('ng'),
	UG('ug'),
    BP('bp'),
    KB('Kb'),
    PM('pm'),
    NM('nm')
	
    final String value

    private UdfUnits(String value) {
        this.value = value
    }

	String toString() {
		return value
	}
}
