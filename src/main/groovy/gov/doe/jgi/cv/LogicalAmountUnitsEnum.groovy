package gov.doe.jgi.cv
/*
    X
    Gb
    M mappable reads
    M reads
    M reads_CCS
    Gb_CCS
    X_CCS
 */

enum LogicalAmountUnitsEnum {
    X('X'),
    GB('Gb'),
    M_MAPPABLE_READS('M mappable reads'),
    M_READS('M reads'),
    M_READS_CCS('M reads_CCS'),
    GB_CCS('Gb_CCS'),
    X_CCS('X_CCS')

    public final String value

    private LogicalAmountUnitsEnum(String value) {
        this.value = value
    }
}
