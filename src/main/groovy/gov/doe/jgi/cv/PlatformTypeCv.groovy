package gov.doe.jgi.cv

enum PlatformTypeCv {
    ILLUMINA('Illumina'),
    PACBIO('PacBio')
	
	private final String value
	
	private PlatformTypeCv(String value) {
		this.value = value
	}

	String getValue() {
		return this.value
	}
	
	String toString() {
		return value
	}
	
	static PlatformTypeCv toEnum(value) {
		return values().find { it.value == value }
	}

	static int toSortIndex(value) {
		if (PACBIO.value == value)
			return 1
		return -1
	}
}
