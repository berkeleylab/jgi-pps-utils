package gov.doe.jgi.cv

import gov.doe.jgi.model.SequencerModelCv

enum SequencerModelTypeCv {
	MISEQ('MiSeq', PlatformTypeCv.ILLUMINA),
	NEXTSEQ_HO('NextSeq-HO', PlatformTypeCv.ILLUMINA), // inactive PC-64
	NEXTSEQ_MO('NextSeq-MO', PlatformTypeCv.ILLUMINA), // inactive PC-64
	NOVASEQ_S4('NovaSeq S4', PlatformTypeCv.ILLUMINA),
	NOVASEQ_SP('NovaSeq SP', PlatformTypeCv.ILLUMINA),
	NOVASEQ_S2('NovaSeq S2', PlatformTypeCv.ILLUMINA),
	NOVASEQX_10B('NovaSeqX 10B', PlatformTypeCv.ILLUMINA),
	NOVASEQX_25B('NovaSeqX 25B', PlatformTypeCv.ILLUMINA),
	NOVASEQX_1_5B('NovaSeqX 1.5B', PlatformTypeCv.ILLUMINA),
	SEQUEL_II('Sequel II', PlatformTypeCv.PACBIO), // inactive PC-53
	SEQUEL_II_E('Sequel IIe', PlatformTypeCv.PACBIO), // inactive PC-138
	REVIO('Revio', PlatformTypeCv.PACBIO),
	X10('X10', PlatformTypeCv.ILLUMINA) // Hudson Alpha

	final String value
	final PlatformTypeCv platformTypeCv

    SequencerModelTypeCv(String value, PlatformTypeCv platform) {
		this.value = value
		this.platformTypeCv = platform
	}

	String getValue() {
		return this.value
	}
	
	String toString() {
		return value
	}

	static SequencerModelTypeCv toEnum(value) {
		return values().find { it.value == value }
	}

	static String getNovaSeqPrefix() {
		return NOVASEQ_S4.value.split(SequencerModelCv.spaceRegex)[0] + ' '
	}

	static String getNovaSeqXPrefix() {
		return NOVASEQX_10B.value.split(SequencerModelCv.spaceRegex)[0]
	}

	static String getSequelPrefix() {
		return SEQUEL_II_E.value.split(SequencerModelCv.spaceRegex)[0]
	}
}
