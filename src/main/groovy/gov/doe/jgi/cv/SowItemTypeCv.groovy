package gov.doe.jgi.cv

enum SowItemTypeCv {
	FRAGMENT(1L, 'Fragment'),
	LMP(2L, 'LMP'),
	RNA(3L, 'RNA'),
	FINISHING(4L, 'Finishing'),
	BLR_AND_RECEIPT(5L, 'Sample Receipt'),
	BLR_AND_RECEIPT_FASTRACK(6L, 'Sample Receipt FasTrack'),
	CUSTOM_ALIQUOT(7L, 'Custom Aliquot'),
	RND(8L, 'RnD'),
	SINGLE_CELL_INTERNAL(9L, 'Fragment - Internal Selection and Amplification'),
	ONBOARDING(10L, 'On Board'),
	SIP_SOURCE_SAMPLE(12L, 'SIP Source Sample'),
	SIP_FRACTIONATED_SAMPLE(13L, 'SIP Fractionated Sample'),
	DAP_SEQ(14L, 'DAP-Seq'),

	final String value
	final Long id

	private SowItemTypeCv(Long id, String sowItemType) {
		this.id = id
		this.value = sowItemType
	}

	String toString() {
		return value
	}
			
	static SowItemTypeCv toEnum(String value) {
		return values().find { it.value == value }
	}

	static int toSortIndex(value) {
		if (FRAGMENT.value == value)
			return 0
		else if (LMP.value == value)
			return 1
		return 2
	}
}