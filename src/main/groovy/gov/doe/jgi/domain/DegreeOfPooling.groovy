package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class DegreeOfPooling {

	@JsonProperty("degree-of-pooling-id")
	Long id
	Integer degreeOfPooling
	String active

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		DegreeOfPooling that = (DegreeOfPooling) o

		if (active != that.active) return false
		if (degreeOfPooling != that.degreeOfPooling) return false
		if (id != that.id) return false

		return true
	}

	int hashCode() {
		int result
		result = id.hashCode()
		result = 31 * result + degreeOfPooling.hashCode()
		result = 31 * result + active.hashCode()
		return result
	}

	static DegreeOfPooling findByDegreeOfPooling(Integer dop) {
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.getDegreeOfPoolingAll().find {
			it.degreeOfPooling == dop
		}
	}

	void setActive(Boolean active) {
		this.active = active ? 'Y' : 'N'
	}

	static DegreeOfPooling read(Long degreeOfPoolingId){
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.getDegreeOfPooling(degreeOfPoolingId)
	}
	
//	public String toString() {
//		return degreeOfPooling?.toString()
//	}
	
}
