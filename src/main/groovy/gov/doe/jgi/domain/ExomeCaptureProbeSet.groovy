package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class ExomeCaptureProbeSet {

	@JsonProperty("exome-capture-probe-set-id")
	Long id

	static ExomeCaptureProbeSet findByExomeCaptureProbeSetName(String s) {
		list().find {
			it.exomeCaptureProbeSetName == s
		}
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		ExomeCaptureProbeSet that = (ExomeCaptureProbeSet) o

		if (active != that.active) return false
		if (exomeCaptureProbeSetName != that.exomeCaptureProbeSetName) return false
		if (id != that.id) return false

		return true
	}

	int hashCode() {
		int result
		result = (id != null ? id.hashCode() : 0)
		result = 31 * result + (exomeCaptureProbeSetName != null ? exomeCaptureProbeSetName.hashCode() : 0)
		result = 31 * result + (active != null ? active.hashCode() : 0)
		return result
	}
	String exomeCaptureProbeSetName
	String active
	void setActive(Boolean active) {
		this.active = active ? 'Y' : 'N'
	}
	
	String toString() {
		return exomeCaptureProbeSetName
	}

	static Collection<ExomeCaptureProbeSet> list() {
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.fetchExomeCaptureProbeSet().values()
	}

	static ExomeCaptureProbeSet read(Long exomeCaptureProbeSetId) {
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.getExomeCaptureProbeSet(exomeCaptureProbeSetId)
	}
}
