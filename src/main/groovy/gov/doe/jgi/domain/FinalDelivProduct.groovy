package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class FinalDelivProduct {

	@JsonProperty("final-deliverable-product-id")
	Long id
	@JsonProperty("final-deliverable-product-name")
	String finalDelivProductName
	String active

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		FinalDelivProduct that = (FinalDelivProduct) o

		if (active != that.active) return false
		if (finalDelivProductName != that.finalDelivProductName) return false
		if (id != that.id) return false

		return true
	}

	int hashCode() {
		int result
		result = id.hashCode()
		result = 31 * result + finalDelivProductName.hashCode()
		result = 31 * result + active.hashCode()
		return result
	}

	void setActive(Boolean active) {
		this.active = active ? 'Y' : 'N'
	}

	static FinalDelivProduct read(Long id){
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.getFinalDelivProduct(id)
	}
	
}
