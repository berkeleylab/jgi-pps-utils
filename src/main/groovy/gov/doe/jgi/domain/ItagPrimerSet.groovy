package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class ItagPrimerSet {

	@JsonProperty("itag-primer-set-id")
	Long id
	String itagPrimerSetName
	String active
	void setActive(Boolean active) {
		this.active = active ? 'Y' : 'N'
	}

    static ItagPrimerSet findByItagPrimerSetName(String s) {
		list().find {
			it.itagPrimerSetName == s
		}
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		ItagPrimerSet that = (ItagPrimerSet) o

		if (active != that.active) return false
		if (id != that.id) return false
		if (itagPrimerSetName != that.itagPrimerSetName) return false

		return true
	}

	int hashCode() {
		int result
		result = id.hashCode()
		result = 31 * result + itagPrimerSetName.hashCode()
		result = 31 * result + active.hashCode()
		return result
	}

	static ItagPrimerSet read(Long itagPrimerSetId) {
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.getItagPrimerSet(itagPrimerSetId)
	}

	static Collection<ItagPrimerSet> list() {
		PcService clarityDataService = BeanUtil.getBean(PcService.class)
		clarityDataService.fetchItagPrimerSet().values()
	}
	
	String toString() {
		itagPrimerSetName
	}

}
