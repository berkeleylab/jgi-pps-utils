package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.ClarityDataService
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class LibraryCreationQueue {

	@JsonProperty("lc-queue-id")
	Long id
	@JsonProperty("lc-queue")
	String libraryCreationQueue
	BigDecimal plateTargetMassLibTrialNg
	BigDecimal plateTargetVolLibTrialUl
	BigDecimal tubeTargetMassLibTrialNg
	BigDecimal tubeTargetVolLibTrialUl

	String toString() {
		return libraryCreationQueue
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		LibraryCreationQueue that = (LibraryCreationQueue) o

		if (id != that.id) return false

		return true
	}

	int hashCode() {
		return id.hashCode()
	}

	static LibraryCreationQueue read(Long libraryCreationQueueId) {
		ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
		clarityDataService.getLibraryCreationQueue(libraryCreationQueueId)
	}
}
