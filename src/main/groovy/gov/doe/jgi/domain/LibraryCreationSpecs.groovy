package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.pi.pps.clarity_node_manager.util.ContainerTypes
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.ClarityDataService
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class LibraryCreationSpecs {

    @JsonProperty("lc-specs-id")
    Long id
    @JsonProperty("lc-specs-active")
    Boolean active
    @JsonProperty("lc-specs")
    String libraryCreationSpecs
    String organizationName
    String libraryProtocol
    Integer targetTemplateSizeBp
    String tightInsert
    String rrnaDepletion
    String polyASelection

    void setTightInsert(Boolean tightInsert) {
        this.tightInsert = tightInsert ? 'Y' : 'N'
    }

    void setRrnaDepletion(Boolean rrnaDepletion) {
        this.rrnaDepletion = rrnaDepletion ? 'Y' : 'N'
    }

    void setPolyASelection(Boolean polyASelection) {
        this.polyASelection = polyASelection ? 'Y' : 'N'
    }
    List<LibraryCreationQueue> lcQueues

    static LibraryCreationSpecs read(Long specsId) {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getLibraryCreationSpecsById(specsId)
    }

    static LibraryCreationSpecs findByName(String specsName) {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getLibraryCreationSpecsByName(specsName)
    }

    String toString() {
        return libraryCreationSpecs
    }

    LibraryCreationQueue getPlateLibraryCreationQueue() {
        lcQueues.find { it.plateTargetMassLibTrialNg && it.plateTargetVolLibTrialUl}
    }

    LibraryCreationQueue getTubeLibraryCreationQueue() {
        lcQueues.find { it.tubeTargetMassLibTrialNg && it.tubeTargetVolLibTrialUl}
    }

    LibraryCreationQueue getSingleLibraryCreationQueue(ContainerTypes containerType){
        if(containerType in [ContainerTypes.WELL_PLATE_96, ContainerTypes.WELL_PLATE_384])
            return plateLibraryCreationQueue
        return tubeLibraryCreationQueue
    }
}