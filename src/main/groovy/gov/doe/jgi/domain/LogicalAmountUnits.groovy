package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class LogicalAmountUnits {

	@JsonProperty("logical-amount-units-id")
	Long id

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		LogicalAmountUnits that = (LogicalAmountUnits) o

		if (active != that.active) return false
		if (id != that.id) return false
		if (logicalAmountUnits != that.logicalAmountUnits) return false

		return true
	}

	int hashCode() {
		int result
		result = id.hashCode()
		result = 31 * result + logicalAmountUnits.hashCode()
		result = 31 * result + active.hashCode()
		return result
	}
	String logicalAmountUnits
	String active

	void setActive(Boolean active) {
		this.active = active ? 'Y' : 'N'
	}
	
	String toString() {
		return logicalAmountUnits
	}


	static LogicalAmountUnits read(Long id){
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.getLogicalAmountUnits(id)
	}

	static Collection<LogicalAmountUnits> list() {
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.fetchLogicalAmountUnits().values()
	}

	static LogicalAmountUnits findByLogicalAmountUnits(String s){
		list().find {
			it.logicalAmountUnits == s
		}
	}

}
