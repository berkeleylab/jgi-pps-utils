package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class MaterialType {

	@JsonProperty("material-type-id")
	Long id
	String materialType

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		MaterialType that = (MaterialType) o

		if (id != that.id) return false
		if (materialCategory != that.materialCategory) return false
		if (materialType != that.materialType) return false

		return true
	}

	int hashCode() {
		int result
		result = id.hashCode()
		result = 31 * result + materialType.hashCode()
		result = 31 * result + (materialCategory != null ? materialCategory.hashCode() : 0)
		return result
	}
	String materialCategory

	static MaterialType read(Long id) {
		PcService pcService = BeanUtil.getBean(PcService.class)
		pcService.getMaterialType(id)
	}

	String toString() {
		return materialType
	}

}
