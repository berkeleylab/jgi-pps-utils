package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.ClarityDataService
import groovy.transform.Canonical

@Canonical
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
class QcType {
	@JsonProperty("qc-type-id")
	Long id
	String qcType
	String active
	void setActive(Boolean active) {
		this.active = active ? 'Y' : 'N'
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		QcType qcType = (QcType) o

		if (id != qcType.id) return false

		return true
	}

	int hashCode() {
		return id.hashCode()
	}

	static QcType read(Long qcTypeId) {
		ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
		clarityDataService.getQcType(qcTypeId)
	}

	static QcType findByQcType(String qcTypeParam) {
		ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
		clarityDataService.fetchQcTypes().values().find({
			it.qcType == qcTypeParam
		})
	}

	static List<QcType> getActiveQcTypes() {
		ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
		clarityDataService.fetchQcTypes().values().findAll({
			it.active == 'Y'
		})
	}
}
