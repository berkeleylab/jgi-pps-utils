package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.config.Organization
import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.ClarityDataService
import groovy.transform.Canonical
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Canonical
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
class RunMode {
    static final Logger log = LoggerFactory.getLogger(RunMode.class)

    @JsonProperty("run-mode-id")
    Long id
    String runMode
    BigInteger readLengthBp
    BigInteger readTotal
    Long sequencerModelId
    String sequencerModel
    Long platformId
    String platform

    @JsonProperty("clarity-lims-queue-id")
    Long workflowId // will be deprecated PPS-5840
    @JsonProperty("clarity-lims-queue-name")
    String workflowName
    String organizationName
    String active

    void setActive(Boolean active) {
        this.active = active ? 'Y' : 'N'
    }

    String toString() {
        runMode
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        RunMode runMode = (RunMode) o

        if (id != runMode.id) return false

        return true
    }

    int hashCode() {
        return id.hashCode()
    }

    boolean isPacBio() {
        platform == 'PacBio'
    }

    boolean isIllumina() {
        platform == 'Illumina'
    }

    static RunMode read(Long runModeId) {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getRunMode(runModeId)
    }

    static RunMode findByRunMode(String runModeParam, Organization organization = Organization.JGI) {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getRunModes().find {
            it.runMode == runModeParam && it.organizationName == organization.name
        }
    }

    static RunMode findByRunModeAndOrganization(String runModeParam, Organization organization) {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getRunModes().find {
            it.runMode == runModeParam && it.organizationName == organization.name
        }
    }

    static List<RunMode> findAllByRunMode(String runModeName) {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getRunModes().findAll {
            it.runMode == runModeName
        }
    }

    static RunMode findByRunModeAndActive(String runModeParam, String active) {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getRunModes().find {
            it.runMode == runModeParam && it.active == active
        }
    }

    static List<RunMode> getActiveRunModes() {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getRunModes().findAll { it.active == 'Y' }
    }

    static Collection<RunMode> list() {
        ClarityDataService clarityDataService = BeanUtil.getBean(ClarityDataService.class)
        clarityDataService.getRunModes()
    }

    WorkflowNode getSequencingWorkflowNode() {
        return workflowName? BeanUtil.nodeManager.getWorkflowNodeByName(workflowName) :
                BeanUtil.nodeManager.getWorkflowNode(workflowId)
    }
}
