package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class SequencingProduct {

    @JsonProperty("sequencing-product-id")
    Long id

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        SequencingProduct that = (SequencingProduct) o

        if (active != that.active) return false
        if (defaultSequencingStrategyId != that.defaultSequencingStrategyId) return false
        if (id != that.id) return false
        if (sequencingProductName != that.sequencingProductName) return false

        return true
    }

    int hashCode() {
        int result
        result = id.hashCode()
        result = 31 * result + sequencingProductName.hashCode()
        result = 31 * result + active.hashCode()
        result = 31 * result + defaultSequencingStrategyId.hashCode()
        return result
    }

    static SequencingProduct findBySequencingProductName(String sequencingProductName) {
        PcService pcService = BeanUtil.getBean(PcService.class)
        pcService.getSequencingProducts().find {
            it.sequencingProductName == sequencingProductName
        }
    }

    static SequencingProduct read(Long id) {
        PcService pcService = BeanUtil.getBean(PcService.class)
        pcService.getSequencingProduct(id)
    }

    SequencingStrategy getDefaultSequencingStrategy() {
        SequencingStrategy.read(defaultSequencingStrategyId)
    }

    String sequencingProductName
    String active
    void setActive(Boolean active) {
        this.active = active ? 'Y' : 'N'
    }

    Long defaultSequencingStrategyId

    String toString() {
        "[${id}] \"${sequencingProductName}\""
    }
}
