package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService
import groovy.transform.ToString

/**
 * Created by iratnere on 1/24/15.
 */
@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class SequencingStrategy {

    @JsonProperty("sequencing-strategy-id")
    Long id
    String sequencingStrategyName
    Long materialTypeId
    String materialTypeName
    String materialCategoryName
    String sampleMappingStrategyName
    Boolean sampleGroupingRequired
    String active
    Long workflowId

    void setActive(Boolean active) {
        this.active = active ? 'Y' : 'N'
    }


    static SequencingStrategy findBySequencingStrategyName(String stratName) {
        PcService pcService = BeanUtil.getBean(PcService.class)
        pcService.getSequencingStrategyByName(stratName)
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        SequencingStrategy that = (SequencingStrategy) o

        if (active != that.active) return false
        if (id != that.id) return false
        if (materialCategoryName != that.materialCategoryName) return false
        if (materialTypeId != that.materialTypeId) return false
        if (materialTypeName != that.materialTypeName) return false
        if (sampleGroupingRequired != that.sampleGroupingRequired) return false
        if (sampleMappingStrategyName != that.sampleMappingStrategyName) return false
        if (sequencingStrategyName != that.sequencingStrategyName) return false
        if (workflowId != that.workflowId) return false

        return true
    }

    int hashCode() {
        int result
        result = id.hashCode()
        result = 31 * result + (sequencingStrategyName != null ? sequencingStrategyName.hashCode() : 0)
        result = 31 * result + (materialTypeId != null ? materialTypeId.hashCode() : 0)
        result = 31 * result + (materialTypeName != null ? materialTypeName.hashCode() : 0)
        result = 31 * result + (materialCategoryName != null ? materialCategoryName.hashCode() : 0)
        result = 31 * result + (sampleMappingStrategyName != null ? sampleMappingStrategyName.hashCode() : 0)
        result = 31 * result + (sampleGroupingRequired != null ? sampleGroupingRequired.hashCode() : 0)
        result = 31 * result + (active != null ? active.hashCode() : 0)
        result = 31 * result + (workflowId != null ? workflowId.hashCode() : 0)
        return result
    }

    static SequencingStrategy read(Long id) {
        PcService pcService = BeanUtil.getBean(PcService.class)
        pcService.getSequencingStrategy(id)
    }

    boolean isInternalSingleCell() {
        id in [87, //Microbial Minimal Draft, Internal Single Cell 1.0
               95//Single Particle Sort, 16S Negative Internal 1.0
        ]*.toLong()
    }


}
