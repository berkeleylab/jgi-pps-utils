package gov.doe.jgi.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import gov.doe.jgi.utils.BeanUtil
import gov.doe.jgi.utils.PcService
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class SowItemType {

    @JsonProperty("sow-item-type-id")
    Long id

    String sowItemType
    String active
    void setActive(Boolean active) {
        this.active = active ? 'Y' : 'N'
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        SowItemType that = (SowItemType) o

        if (active != that.active) return false
        if (id != that.id) return false
        if (sowItemType != that.sowItemType) return false

        return true
    }

    int hashCode() {
        int result
        result = id.hashCode()
        result = 31 * result + sowItemType.hashCode()
        result = 31 * result + active.hashCode()
        return result
    }

    String toString() {
        sowItemType
    }

    boolean isStopAtReceipt() {
        [5L,6L].contains(this.id) //'BLR and Receipt', 'BLR and Receipt FasTrack'
    }

    boolean isInternalSingleCell() {
        id == 9 //'Single Cell Internal'
    }

    boolean isDapSeq() {
        id == 14 //'DAP-Seq'
    }

    boolean isSipFractionatedSample() {
        id == 13 //SIP Fractionated Sample
    }

    boolean isSipSourceSample() {
        id == 12 //SIP Source Sample
    }

    boolean isOnBoard() {
        id == 10 // 'On Board'
    }

    static SowItemType read(Long sowItemTypeId){
        PcService pcService = BeanUtil.getBean(PcService.class)
        pcService.getSowItemType(sowItemTypeId)
    }
}
