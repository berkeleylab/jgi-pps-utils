package gov.doe.jgi.freezer

class FreezerContainerMapper {

    static List<FreezerContainer> toFreezerContainerList(List<FreezerDto> freezerDtoList) {
        List<String> barcodes = freezerDtoList*.barcode.unique()
        //DW WS: If the container can be found in multiple freezers, multiple records are returned for the same barcode.
        //Display the list of locations in Excel
        List<FreezerContainer> freezerDtoNoDuplicates = barcodes.collect{ String barcode ->
            FreezerContainer freezerContainer = new FreezerContainer(
                    barcode: barcode,
                    freezerLocations: [],
                    location: ''
            )
            freezerDtoList.findAll{it.barcode == barcode && it.freezerLocation}
                    .each{freezerContainer.freezerLocations.add(it.freezerLocation)}
            String location = freezerContainer.freezerLocations?.join('; ')
            freezerContainer.location = location
            freezerContainer
        }
        return freezerDtoNoDuplicates
    }
}
