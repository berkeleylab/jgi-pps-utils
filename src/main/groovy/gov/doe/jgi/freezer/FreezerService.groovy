package gov.doe.jgi.freezer

import com.fasterxml.jackson.databind.json.JsonMapper
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient

@Service
class FreezerService {
	static final Logger logger = LoggerFactory.getLogger(FreezerService.class)
	static final String PATH_FREEZER = "dw-container-freezer-locations/"

	@Autowired
	WebClient wsAccessClient

	/*
     * Call DW Container Freezer Location web service.
     * Please see API Contract https://docs.jgi.doe.gov/display/DW/Container+Freezer+Location
     * Response contains freezer locations from any freezers (Stirling, SAM, SAM HD, etc.).
     * If the container can not be found for a given barcode, the freezer location is set to "Location Unavailable".
     * If the container can be found in multiple freezers, multiple records are returned for the same barcode.
     */
	List<FreezerDto> getContainerFreezerLocations(List<String> barcodes) {
		if (!barcodes)
			return null
		String barcodesToJson = JsonMapper.builder().build().writeValueAsString(barcodes)
		logger.info "Request: BODY $barcodesToJson"
		List<FreezerDto> webClientResponse = wsAccessClient.post()
				.uri(PATH_FREEZER)
				.contentType(MediaType.APPLICATION_JSON)
				.body(BodyInserters.fromValue(barcodesToJson))
				.retrieve()
				.bodyToFlux(FreezerDto)
				.collectList()
				.block()
		//logger.info "RESPONSE List<${FreezerDto}>: ${JsonOutput.prettyPrint(JsonOutput.toJson(webClientResponse))}"
		return webClientResponse
	}

	/**
	 * Call DW Container Freezer Location web service.
	 * Look up the input containers in freezers(Stirling, SAM, SAM HD, etc.)
	 * @return
	 * List of FreezerDto objects. Each FreezerDto object contains barcode and freezer location strings.
	 *
	 */
	List<FreezerContainer> freezerLookupInputAnalytes(ProcessNode processNode) {
		List<String> barcodes = processNode.inputAnalytes*.containerNode.unique()*.name
		List<FreezerDto> freezerLocations = getContainerFreezerLocations(barcodes)
		return FreezerContainerMapper.toFreezerContainerList(freezerLocations)
	}
}
