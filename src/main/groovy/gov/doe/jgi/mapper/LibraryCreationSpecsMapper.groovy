package gov.doe.jgi.mapper

import gov.doe.jgi.domain.LibraryCreationQueue
import gov.doe.jgi.domain.LibraryCreationSpecs
import gov.doe.jgi.model.LibraryCreationQueueCv
import gov.doe.jgi.model.LibraryCreationSpecsCv

class LibraryCreationSpecsMapper {
    static LibraryCreationSpecsCv toLibraryCreationSpecsCv(LibraryCreationSpecs lcSpecsDto) {
        LibraryCreationSpecsCv lcSpecs = new LibraryCreationSpecsCv(
                id: lcSpecsDto.id,
                libraryCreationSpecs: lcSpecsDto.libraryCreationSpecs,
                active: lcSpecsDto.active,
                organizationName: lcSpecsDto.organizationName
        )
        lcSpecs.libraryCreationQueues = lcSpecsDto.lcQueues.collect { LibraryCreationQueue queue ->
            new LibraryCreationQueueCv(
                    id: queue.id,
                    libraryCreationQueue: queue.libraryCreationQueue,
                    libraryCreationSpecs: lcSpecs,
                    plateTargetMassLibTrialNg: queue.plateTargetMassLibTrialNg,
                    plateTargetVolumeLibTrialUl: queue.plateTargetVolLibTrialUl,
                    tubeTargetMassLibTrialNg: queue.tubeTargetMassLibTrialNg,
                    tubeTargetVolumeLibTrialUl: queue.tubeTargetVolLibTrialUl
            )
        }
        return lcSpecs
    }
}
