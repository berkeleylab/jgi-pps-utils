package gov.doe.jgi.mapper

import gov.doe.jgi.domain.QcType
import gov.doe.jgi.model.QcTypeCv

class QcTypeCvMapper {
    static QcTypeCv toQcTypeCv(QcType qcTypeDto) {
        if (!qcTypeDto)
            return null
        QcTypeCv qcTypeCv = new QcTypeCv(
                active: (qcTypeDto.active == 'Y'),
                id: qcTypeDto.id,
                qcType: qcTypeDto.qcType
        )
        return qcTypeCv
    }

    static List<QcTypeCv> toQcTypeCvList(List<QcType> qcTypes) {
        if (!qcTypes)
            return []
        return qcTypes.collect {toQcTypeCv(it)}
    }
}
