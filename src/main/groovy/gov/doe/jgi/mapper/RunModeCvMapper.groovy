package gov.doe.jgi.mapper

import gov.doe.jgi.domain.RunMode
import gov.doe.jgi.model.PlatformCv
import gov.doe.jgi.model.RunModeCv
import gov.doe.jgi.model.SequencerModelCv

class RunModeCvMapper {
    static RunModeCv toRunModeCv(RunMode runModeDto) {
        if (!runModeDto)
            return null
        PlatformCv platformCv = new PlatformCv(
                id: runModeDto.platformId,
                platform: runModeDto.platform
        )
        SequencerModelCv sequencerModelCv = new SequencerModelCv(
                id: runModeDto.sequencerModelId,
                sequencerModel: runModeDto.sequencerModel,
                platformCv: platformCv,
        )
        RunModeCv runModeCv = new RunModeCv(
                runMode: runModeDto.runMode,
                id: runModeDto.id,
                active: (runModeDto.active == 'Y'),
                organizationName: runModeDto.organizationName,
                readLengthBp: runModeDto.readLengthBp,
                readTotal: runModeDto.readTotal,
                sequencerModel: sequencerModelCv,
                clarityLimsQueueName: runModeDto.workflowName
        )
        return runModeCv
    }

    static List<RunModeCv> toRunModeCvList(Collection<RunMode> runModes) {
        if (!runModes)
            return []
        return runModes.collect {toRunModeCv(it)}
    }
}
