package gov.doe.jgi.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import groovy.transform.Canonical

@Canonical
@JsonIgnoreProperties(ignoreUnknown = true)
class ClarityActiveWorkflow {
    int id
    String name
    String workflowUri
    List<WorkflowStage> stages
}
