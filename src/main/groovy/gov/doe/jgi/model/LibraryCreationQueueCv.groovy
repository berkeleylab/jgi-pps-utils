package gov.doe.jgi.model

import gov.doe.jgi.config.Organization

class LibraryCreationQueueCv {
	static final String LIBRARY_CREATION_WORKFLOW_PREFIX = 'LC '
	int id
	String libraryCreationQueue
	BigDecimal plateTargetVolumeLibTrialUl
	BigDecimal tubeTargetVolumeLibTrialUl
	BigDecimal plateTargetMassLibTrialNg
	BigDecimal tubeTargetMassLibTrialNg

	LibraryCreationSpecsCv libraryCreationSpecs

	boolean getIsPlateQueue() {
		switch(organization) {
			case Organization.JGI: return (plateTargetVolumeLibTrialUl && plateTargetMassLibTrialNg && active)
			default: return true
		}
	}

	boolean getIsTubeQueue() {
		switch(organization) {
			case Organization.JGI: return (tubeTargetVolumeLibTrialUl && tubeTargetMassLibTrialNg && active)
			default: return true
		}
	}

	boolean getActive() {
		libraryCreationSpecs.active
	}

	Organization getOrganization() {
		libraryCreationSpecs.organization
	}

	boolean equals(LibraryCreationQueueCv lcq) {
		return this.id == lcq.id
	}

	int hashCode() {
		return id
	}

	String getLcWorkflowName() {
		return LIBRARY_CREATION_WORKFLOW_PREFIX + libraryCreationQueue
	}
}