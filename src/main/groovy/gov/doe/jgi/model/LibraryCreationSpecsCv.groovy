package gov.doe.jgi.model

import gov.doe.jgi.config.Organization

class LibraryCreationSpecsCv {
	boolean active
	BigInteger id
	String libraryCreationSpecs
	String organizationName

	List<LibraryCreationQueueCv> libraryCreationQueues

	String toString() {
		return libraryCreationSpecs
	}

	LibraryCreationQueueCv plateLibraryCreationQueue() {
		libraryCreationQueues.find {it.isPlateQueue }
	}

	LibraryCreationQueueCv tubeLibraryCreationQueue() {
		libraryCreationQueues.find {it.isTubeQueue }
	}

	Organization getOrganization() {
		Organization.toEnum(organizationName)
	}

	boolean equals(LibraryCreationSpecsCv lcs) {
		return this.id == lcs.id
	}

	int hashCode() {
		return id.hashCode()
	}
}