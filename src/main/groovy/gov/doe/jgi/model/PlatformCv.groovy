package gov.doe.jgi.model

import gov.doe.jgi.cv.PlatformTypeCv
import groovy.transform.Canonical

@Canonical
class PlatformCv {
    Long id
    String platform

    boolean getIsPacBio() {
        platform == PlatformTypeCv.PACBIO.value
    }

    boolean getIsIllumina() {
        platform == PlatformTypeCv.ILLUMINA.value
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        PlatformCv that = (PlatformCv) o

        if (id != that.id) return false

        return true
    }

    int hashCode() {
        return id.hashCode()
    }
}
