package gov.doe.jgi.model

import gov.doe.jgi.domain.QcType
import gov.doe.jgi.mapper.QcTypeCvMapper
import groovy.transform.Canonical

@Canonical
class QcTypeCv {
	Long id
	String qcType
	boolean active

	static QcTypeCv get(Long qcTypeId) {
		return QcTypeCvMapper.toQcTypeCv(QcType.read(qcTypeId))
	}
	static QcTypeCv findByQcType(String qcType) {
		return QcTypeCvMapper.toQcTypeCv(QcType.findByQcType(qcType))
	}
	static List<QcTypeCv> getActiveQcTypes() {
		QcTypeCvMapper.toQcTypeCvList(QcType.activeQcTypes)
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		QcTypeCv qcTypeCv = (QcTypeCv) o

		if (id != qcTypeCv.id) return false

		return true
	}

	int hashCode() {
		return id.hashCode()
	}
}