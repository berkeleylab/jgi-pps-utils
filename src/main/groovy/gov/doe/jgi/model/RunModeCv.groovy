package gov.doe.jgi.model

import gov.doe.jgi.config.Organization
import gov.doe.jgi.domain.RunMode
import gov.doe.jgi.mapper.RunModeCvMapper
import gov.doe.jgi.pi.pps.clarity_node_manager.node.WorkflowNode
import gov.doe.jgi.utils.BeanUtil
import groovy.transform.Canonical

@Canonical
class RunModeCv implements Comparator<RunModeCv> {
	String organizationName
	Long id
	String runMode
	SequencerModelCv sequencerModel
	String clarityLimsQueueName
	boolean active
	BigInteger readLengthBp
	BigInteger readTotal

	static List<RunModeCv> getAllRunModes() {
		return RunModeCvMapper.toRunModeCvList(RunMode.list())
	}

	static RunModeCv findByRunMode(String runModeName, Organization organization = Organization.JGI) {
		return RunModeCvMapper.toRunModeCv(RunMode.findByRunMode(runModeName, organization))
	}

	static List<RunModeCv> findAllByRunMode(String runModeName) {
		return RunModeCvMapper.toRunModeCvList(RunMode.findAllByRunMode(runModeName))
	}

	static List<RunModeCv> getActiveRunModes() {
		return RunModeCvMapper.toRunModeCvList(RunMode.getActiveRunModes())
	}

	WorkflowNode getSequencingWorkflowNode() {
		return BeanUtil.nodeManager.getWorkflowNodeByName(clarityLimsQueueName)
	}

	String getSequencingWorkflowUri() {
		sequencingWorkflowNode?.workflowUri
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		RunModeCv runModeCv = (RunModeCv) o

		if (id != runModeCv.id) return false

		return true
	}

	int hashCode() {
		return id.hashCode()
	}

	@Override
	int compare(RunModeCv a, RunModeCv b) {
		return a.runMode.compareToIgnoreCase(b.runMode)
	}

	boolean getIsPacBio(){
		return this.sequencerModel.isPacBio
	}

	boolean getIsIllumina(){
		return this.sequencerModel.isIllumina
	}

	String toString() {
		runMode
	}
}