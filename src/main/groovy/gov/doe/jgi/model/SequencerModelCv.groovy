package gov.doe.jgi.model

import gov.doe.jgi.cv.SequencerModelTypeCv
import groovy.transform.Canonical

@Canonical
class SequencerModelCv {
	static final String NOVASEQ = SequencerModelTypeCv.novaSeqPrefix
	static final String NOVASEQX = SequencerModelTypeCv.novaSeqXPrefix
	static final String SEQUEL = SequencerModelTypeCv.sequelPrefix
	static final def spaceRegex = "\\s+"

	Long id
	String sequencerModel
	PlatformCv platformCv

	String getPlatform() {
		return platformCv.platform
	}

	boolean getIsSequel() {
		sequencerModel?.contains(SEQUEL)
	}

	boolean getIsRevio() {
		sequencerModel.contains(SequencerModelTypeCv.REVIO.value)
	}

	boolean getIsPacBio() {
		platformCv?.isPacBio
	}

	boolean getIsIllumina() {
		platformCv?.isIllumina
	}

	boolean getIsIlluminaMiSeq() {
		sequencerModel == SequencerModelTypeCv.MISEQ.value
	}

	boolean getIsIlluminaNovaSeq() {
		sequencerModel.contains(NOVASEQ) // NovaSeqX seq models are not included
	}

	boolean getIsIlluminaNovaSeqX() {
		sequencerModel.contains(NOVASEQX)
	}

    boolean getIsIlluminaNextSeqHO() {
        sequencerModel == SequencerModelTypeCv.NEXTSEQ_HO.value
    }

    boolean getIsNextSeq() {
        return isIlluminaNextSeqHO || isIlluminaNextSeqMO
    }

    boolean getIsIlluminaNextSeqMO() {
        sequencerModel == SequencerModelTypeCv.NEXTSEQ_MO.value
    }

	String toString() {
		sequencerModel
	}

	String getFlowcellType() {
		if(isIlluminaNovaSeq || isIlluminaNovaSeqX) {
			String[] sequencerModelSplit = sequencerModel.split(spaceRegex)
			if(sequencerModelSplit.size() <= 1)
				return ""
			return sequencerModelSplit[1]
		}
		return ""
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.class) return false

		SequencerModelCv that = (SequencerModelCv) o

		if (id != that.id) return false

		return true
	}

	int hashCode() {
		return id.hashCode()
	}
}