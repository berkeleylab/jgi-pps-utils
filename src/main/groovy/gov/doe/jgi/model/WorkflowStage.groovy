package gov.doe.jgi.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.StepConfigurationNode
import groovy.transform.Canonical

@Canonical
@JsonIgnoreProperties(ignoreUnknown = true)
class WorkflowStage {
    String name
    String stageUri
    String processType
    String protocolUri
    String stepUri

    StepConfigurationNode getStepConfigurationNode(NodeManager nodeManager) {
        String url = stepUri
        String id = url.replace(nodeManager.nodeConfig.configurationUrl, "")
        return (StepConfigurationNode) nodeManager.getGeneusNode(id,url,StepConfigurationNode)
    }
}
