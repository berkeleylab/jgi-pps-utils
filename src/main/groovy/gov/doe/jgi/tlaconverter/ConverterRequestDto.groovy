package gov.doe.jgi.tlaconverter

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import com.fasterxml.jackson.databind.json.JsonMapper
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
class ConverterRequestDto {
    List logicalAmountUnits = []
    /*
    {"logical-amount-units": [{"library-name": "GTHPP", "from-logical-amount": 0.25, "from-logical-amount-units": "M reads", "to-logical-amount-units": "Gb"}]}
     */
    String toJsonBody() {
        return JsonMapper.builder().build().writeValueAsString(this)
    }
}
