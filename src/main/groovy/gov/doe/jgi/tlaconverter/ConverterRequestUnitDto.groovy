package gov.doe.jgi.tlaconverter

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
class ConverterRequestUnitDto {
    String libraryName
    Long sowItemId
    BigDecimal fromLogicalAmount
    String fromLogicalAmountUnits
    String toLogicalAmountUnits
}
