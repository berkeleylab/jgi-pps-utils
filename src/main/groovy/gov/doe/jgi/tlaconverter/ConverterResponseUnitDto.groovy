package gov.doe.jgi.tlaconverter

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.annotation.JsonNaming
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonNaming(PropertyNamingStrategies.KebabCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
class ConverterResponseUnitDto {
    String libraryName
    BigDecimal fromLogicalAmount
    String fromLogicalAmountUnits
    String toLogicalAmountUnits
    Integer index
    BigDecimal toLogicalAmount
    Long sowItemId
    BigDecimal targetLogicalAmount
    String logicalAmountUnits
/*  def runMode     // "PacBio Sequel 1 X 1200",
    def readTotal     // 1,
    def readLengthBp     // 1200,
    def numberOfReadsBp     // 250000,
    def genomeSizeEstimatedMb     // null,
    def mappableYieldRate     // null
*/
}
