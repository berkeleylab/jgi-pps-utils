package gov.doe.jgi.tlaconverter

import gov.doe.jgi.cv.LogicalAmountUnitsEnum
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

import static groovy.json.JsonOutput.prettyPrint
import static groovy.json.JsonOutput.toJson

@Service
class ConverterService {
    static final Logger logger = LoggerFactory.getLogger(this.class.simpleName)
    static final String GB = LogicalAmountUnitsEnum.GB.value
    static final String tlaConverterUrl = "pps-logical-amount-units-converter"
    @Autowired
    WebClient wsAccessClient

    ConverterResponseUnitDto[] convertUnits(ConverterRequestUnitDto[] requestUnits) {
        if (!requestUnits)
            return null
        String json = new ConverterRequestDto(logicalAmountUnits: requestUnits).toJsonBody()
        logger.info "submission: ${prettyPrint(json)}"
        Mono<ConverterResponseDto> response = wsAccessClient.post()
                .uri(tlaConverterUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(json))
                .retrieve()
                .bodyToMono(ConverterResponseDto.class).log()
        ConverterResponseDto responseModel = response.block()
        logger.info "RESPONSE: ${prettyPrint(toJson(responseModel?.logicalAmountUnits))}"
        //logger.debug "RESPONSE body: ${responseModel?.logicalAmountUnits}"
        return responseModel?.logicalAmountUnits
    }

    ConverterResponseUnitDto[] convertUnitsToGb(ConverterRequestUnitDto[] requestUnits) {
        //get TLA by library stock name, convert TLA to GB
        ConverterResponseUnitDto[] convertedUnits = convertUnits(requestUnits)
        return convertedUnits
    }

    ConverterResponseUnitDto[] convertCoverageToGb(ConverterRequestUnitDto[] requestUnits) {
        // convert CLA to GB
        ConverterRequestUnitDto[] unitsToConvert = requestUnits.findAll{
            (it.fromLogicalAmountUnits != GB && it.fromLogicalAmount)
        }
        ConverterResponseUnitDto[] response = []
        ConverterRequestUnitDto[] unitsInGb = requestUnits.findAll{it.fromLogicalAmountUnits == GB}
        if (unitsInGb) {
            response += unitsInGb.collect{
                new ConverterResponseUnitDto(
                        libraryName: it.libraryName,
                        toLogicalAmountUnits: it.fromLogicalAmountUnits,
                        toLogicalAmount: it.fromLogicalAmount
                )
            }
        }
        ConverterResponseUnitDto[] convertedUnits = convertUnits(unitsToConvert)
        if (convertedUnits) response += convertedUnits
        return response
    }
}
