package gov.doe.jgi.utils

import gov.doe.jgi.config.Organization
import gov.doe.jgi.domain.LibraryCreationQueue
import gov.doe.jgi.domain.LibraryCreationSpecs
import gov.doe.jgi.domain.QcType
import gov.doe.jgi.domain.RunMode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ArtifactNode
import gov.doe.jgi.pi.pps.clarity_node_manager.node.NodeManager
import gov.doe.jgi.pi.pps.clarity_node_manager.node.ProcessNode
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.namedparam.SqlParameterSource
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.reactive.function.client.WebClient

import java.time.Duration

@Transactional
class ClarityDataService {
    static final Logger log = LoggerFactory.getLogger(ClarityDataService.class)
    static final String ORG_QUERY_PARAM = "organization-name="
    static final String PATH_LC_SPECS = "dw-pc-library-creation-specs/"
    static final String PATH_LC_QUEUES = "dw-pc-library-creation-queues/"

    @Autowired
    @Qualifier("limsJdbcTemplate")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate

    @Autowired
    WebClient wsAccessClient
    
    static final String CLARITY_LIMS = 'public'

    private final static String PROCESS_TYPE_NAME_FOR_ID = """
SELECT ptype.DISPLAYNAME,
  p.LUID
FROM ${CLARITY_LIMS}.process p
JOIN ${CLARITY_LIMS}.PROCESSTYPE ptype
ON ptype.TYPEID = p.TYPEID
WHERE p.LUID IN (:prcLimsIDs)
"""
    private final static String INPUT_ANALYTES_IDS_FOR_PROCESS_ID_AND_OUTPUT_ART_ID = """
SELECT art.LUID
FROM ${CLARITY_LIMS}.process p
JOIN ${CLARITY_LIMS}.processiotracker piot
ON piot.PROCESSID = p.PROCESSID
JOIN ${CLARITY_LIMS}.artifact art
ON art.ARTIFACTID = piot.INPUTARTIFACTID
JOIN ${CLARITY_LIMS}.outputmapping om
ON om.trackerid = piot.trackerid
JOIN ${CLARITY_LIMS}.artifact artout
ON artout.ARTIFACTID     = om.outputartifactid
WHERE art.ARTIFACTTYPEID = 2
AND p.LUID  = :prcLimsId
AND artout.LUID  = :outLimsId
"""


    private final static String INPUT_ANALYTES_SAMPLE_IDS_FOR_PROCESS_ID_AND_OUTPUT_ART_ID = """
SELECT art.LUID,
  sp.LUID sampleLimsID
FROM ${CLARITY_LIMS}.process p
JOIN ${CLARITY_LIMS}.processiotracker piot
ON piot.PROCESSID = p.PROCESSID
JOIN ${CLARITY_LIMS}.artifact art
ON art.ARTIFACTID = piot.INPUTARTIFACTID
JOIN ${CLARITY_LIMS}.outputmapping om
ON om.trackerid = piot.trackerid
JOIN ${CLARITY_LIMS}.artifact artout
ON artout.ARTIFACTID = om.outputartifactid
JOIN ${CLARITY_LIMS}.artifact_sample_map am
ON am.artifactid = art.artifactid
JOIN ${CLARITY_LIMS}.process sp
ON am.processid          = sp.processid
WHERE art.ARTIFACTTYPEID = 2
AND p.LUID  = :prcLimsId
AND artout.LUID  = :outLimsId
"""


    List<String> getInputAnalytesIdsForProcessIdAndOutPutArtId(String prcLimsId, String outLimsId) {
        SqlParameterSource parameters = new MapSqlParameterSource().addValue('prcLimsId', prcLimsId).addValue('outLimsId', outLimsId)
        List<String> result = namedParameterJdbcTemplate.queryForList(INPUT_ANALYTES_IDS_FOR_PROCESS_ID_AND_OUTPUT_ART_ID, parameters, String.class)
        log.debug "retrieved ${result?.size()} analytes"
        result
    }


    List<ArtifactNode> getInputAnalytesIdsForOutPutAnalyte(ArtifactNode poolNode) {
        ProcessNode processNode = poolNode.parentProcessNode
        List<String> inputAnalytesIds = getInputAnalytesIdsForProcessIdAndOutPutArtId(processNode.id, poolNode.id)
        NodeManager nodeManager = processNode.nodeManager
        nodeManager.getArtifactNodes(inputAnalytesIds)
    }


    Map<String, String> findProcessTypeForId(Collection<String> prcLimsIDs) {
        assert prcLimsIDs
        Map<String, String> result = [:]
        MapSqlParameterSource parameters = new MapSqlParameterSource()
        parameters.addValue("prcLimsIDs", prcLimsIDs)
        List<Map<String, Object>> resp = namedParameterJdbcTemplate.queryForList(PROCESS_TYPE_NAME_FOR_ID, parameters)
        resp.each { Map<String, Object> m ->
            result[m['LUID'] as String] = m['DISPLAYNAME'] as String
        }
        log.debug "retrieved ${result?.size()} process types: $result"
        result
    }

    private final static INDEX_QUERY = """
SELECT rt.NAME as INDEX_NAME, rtrim(replace(rt.metadata,
'<properties type= "basic"><property name = "SEQUENCE"><![CDATA[s\$Sequence\$0\$h\$f\$',''), ']]></property></properties>')
as SEQUENCE
FROM ${CLARITY_LIMS}.REAGENTTYPE rt
WHERE rt.NAME like 
"""


    private Map<Long, LibraryCreationSpecs> libraryCreationSpecsCached = new HashMap<>()
    private Map<Long, LibraryCreationSpecs> libraryCreationSpecsNameCached = new HashMap<>()

    private Map<Long, LibraryCreationQueue> libraryCreationQueuesCached = new HashMap<>()

    private Map<Long, RunMode> runModesCached

    RunMode getRunMode(Long runModeId) {
        fetchRunModes()[runModeId]
    }

    private Map<Long, QcType> qcTypesCached

    QcType getQcType(Long qcTypeId) {
        fetchQcTypes()[qcTypeId]
    }

    Map<Long, QcType> fetchQcTypes() {
        if (qcTypesCached)
            return qcTypesCached
        String path = "dw-pc-qc-types-cv/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<QcType> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<QcType>>() {}).timeout(Duration.ofSeconds(60)).block()

        qcTypesCached = new HashMap<>()
        content.each { QcType qcType ->
            qcTypesCached[qcType.id] = qcType
        }

        log.debug "retrieved ${qcTypesCached?.size()} qc types"
        return qcTypesCached
    }

    Map<Long, RunMode> fetchRunModes() {
        if (runModesCached)
            return runModesCached
        String path = "dw-pc-run-mode/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<RunMode> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<RunMode>>() {}).timeout(Duration.ofSeconds(60)).block()

        runModesCached = new HashMap<>()
        content.each { RunMode runMode ->
            runModesCached[runMode.id] = runMode
        }

        log.debug "retrieved ${runModesCached?.size()} run modes"
        return runModesCached
    }

    Collection<RunMode> getRunModes() {
        fetchRunModes().values()
    }

    List<RunMode> getRunModesByOrganization(Organization org) {
        fetchRunModes().values().findAll{it.organizationName == org.name}
    }

    LibraryCreationSpecs getLibraryCreationSpecsById(Long specsId) {
        if (specsId == null)
            return null
        LibraryCreationSpecs specs = libraryCreationSpecsCached[specsId]
        if (specs) {
            log.debug "Cached LibraryCreationSpecs by id={$specsId}" //: ${prettyPrint(toJson(specs))}"
            return specs
        }
        String path = PATH_LC_SPECS
        log.debug "$path/$specsId"
        specs = wsAccessClient.get()
                .uri("$path/{id}", specsId)
                .retrieve()
                .bodyToMono(LibraryCreationSpecs.class)
                .block()
        log.debug "RESPONSE LibraryCreationSpecs by id={$specsId}: $specs"
        libraryCreationSpecsCached[specsId] = specs
        return specs

    }


    LibraryCreationSpecs getLibraryCreationSpecsByName(String specsName) {
        if (specsName == null)
            return null
        LibraryCreationSpecs specs = libraryCreationSpecsNameCached[specsName]
        if (specs) {
            log.debug "Cached LibraryCreationSpecs by name={$specsName}" //: ${prettyPrint(toJson(specs))}"
            return specs
        }
        String path = PATH_LC_SPECS
        log.debug "$path/$specsName"
        specs = wsAccessClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("$path")
                        .queryParam("lc-specs", specsName)
                        .build())
                .retrieve()
                .bodyToFlux(LibraryCreationSpecs.class)
                .next()
                .block()
        log.info "RESPONSE LibraryCreationSpecs by name={$specsName}: $specs"
        libraryCreationSpecsNameCached[specsName] = specs
        return specs

    }

    void fetchLibraryCreationQueues() {
        assert libraryCreationQueuesCached.isEmpty()
        String path = PATH_LC_QUEUES
        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<LibraryCreationQueue> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<LibraryCreationQueue>>() {
                }).timeout(Duration.ofSeconds(60)).block()

        content.each { LibraryCreationQueue libraryCreationQueue ->
            libraryCreationQueue.each {
                libraryCreationQueuesCached[it.id] = it
            }
        }
        log.debug "retrieved ${libraryCreationQueuesCached?.size()} library creation queues"
    }

    LibraryCreationQueue getLibraryCreationQueue(Long libraryCreationQueueId) {
        if (libraryCreationQueueId == null)
            return null
        if (libraryCreationQueuesCached.isEmpty()) {
            fetchLibraryCreationQueues()
            return getLibraryCreationQueue(libraryCreationQueueId)
        }
        libraryCreationQueuesCached[libraryCreationQueueId]
    }

    List webClientResponse(String url, Class elementClass) {
        List result = wsAccessClient.get()
                .uri(url)
                .retrieve()
                .bodyToFlux(elementClass)
                .collectList()
                .block()
        log.debug "RESPONSE List<${elementClass.simpleName}>: $result)}"
        return result
    }

    List<LibraryCreationSpecs> getLibraryCreationSpecs(Organization org) {
        String url = "$PATH_LC_SPECS?$ORG_QUERY_PARAM${org?.name}"
        return webClientResponse(url, LibraryCreationSpecs.class) as List<LibraryCreationSpecs>
    }
}
