package gov.doe.jgi.utils

import gov.doe.jgi.model.ClarityActiveWorkflow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

@Service
class ClarityWorkflowService {
    @Autowired
    WebClient wsAccessClient

    String path = "pps-active-clarity-workflows"
    static final String NAME = "name"
    static final String PROCESS_TYPE = "process-type"

    ClarityActiveWorkflow getClarityWorkflow(int workflowId) {
        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path("$path/$workflowId")
                            .build()
                })
        ClarityActiveWorkflow content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<ClarityActiveWorkflow>() {}).block()
        return content
    }

    ClarityActiveWorkflow getClarityWorkflow(String workflowName) {
        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path).queryParam(NAME, workflowName)
                            .build()
                })
        List<ClarityActiveWorkflow> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<ClarityActiveWorkflow>>() {}).block()
        assert content.size() == 1
        return content.first()
    }

    List<ClarityActiveWorkflow> getClarityWorkflows(String processType=null) {
        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                    if(processType)
                        uriBuilder.queryParam(PROCESS_TYPE, processType)
                    uriBuilder.build()
                })
        List<ClarityActiveWorkflow> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<ClarityActiveWorkflow>>() {}).block()
        return content
    }
}
