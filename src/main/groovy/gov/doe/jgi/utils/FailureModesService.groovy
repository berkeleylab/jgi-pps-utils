package gov.doe.jgi.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

import java.time.Duration

@Service
class FailureModesService {

    @Autowired
    WebClient wsAccessClient

    List<String> fetchActiveFailureModes(String processType) {
        String path = "pps-failure-modes"
        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .queryParam("processtype", processType)
                            .build()
                }) as WebClient.RequestHeadersUriSpec
        List<String> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<String>>() {}).timeout(Duration.ofSeconds(60)).block()
        return content as List<String>
    }
}
