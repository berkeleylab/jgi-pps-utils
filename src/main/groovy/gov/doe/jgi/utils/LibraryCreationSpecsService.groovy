package gov.doe.jgi.utils

import gov.doe.jgi.config.Organization
import gov.doe.jgi.domain.LibraryCreationSpecs
import gov.doe.jgi.mapper.LibraryCreationSpecsMapper
import gov.doe.jgi.model.LibraryCreationQueueCv
import gov.doe.jgi.model.LibraryCreationSpecsCv
import org.springframework.beans.factory.annotation.Autowired

class LibraryCreationSpecsService {
    @Autowired
    ClarityDataService clarityDataService

    private Map<Organization, List<LibraryCreationSpecsCv>> libraryCreationSpecsCvs

    List<LibraryCreationSpecsCv> getLibraryCreationSpecs(Organization organization = Organization.JGI) {
        registerSpecs(organization)
        return libraryCreationSpecsCvs[organization]
    }

    LibraryCreationSpecsCv getLibraryCreationSpecs(BigInteger lcSpecsId, Organization organization=Organization.JGI) {
        return getLibraryCreationSpecs(organization).find { it.id == lcSpecsId }
    }

    LibraryCreationSpecsCv getLibraryCreationSpecs(String lcSpecsName, Organization organization=Organization.JGI) {
        List<LibraryCreationSpecsCv> lcSpecs = getLibraryCreationSpecs(organization).findAll {
            it.libraryCreationSpecs == lcSpecsName
        }
        if(!lcSpecs)
            return null
        LibraryCreationSpecsCv lcs = lcSpecs.find { it.active }
        if(!lcs) {
            lcs = lcSpecs.sort {a,b -> a.id <=> b.id }.last()
        }
        return lcs
    }

    List<LibraryCreationQueueCv> getLibraryCreationQueues(Organization organization=Organization.JGI) {
        return getLibraryCreationSpecs(organization)
                .collect{ it.libraryCreationQueues }
                .flatten() as List<LibraryCreationQueueCv>
    }

    LibraryCreationQueueCv getLibraryCreationQueue(String lcQueueName, Organization organization=Organization.JGI) {
        List<LibraryCreationQueueCv> lcQueues = getLibraryCreationQueues(organization).findAll {
            it.libraryCreationQueue == lcQueueName
        }
        if(!lcQueues) {
            return null
        }
        LibraryCreationQueueCv lcq = lcQueues.find { it.active }
        if(!lcq) {
            lcq = lcQueues.sort {a,b -> a.id <=> b.id }.last()
        }
        return lcq
    }

    LibraryCreationQueueCv getLibraryCreationQueue(BigInteger lcQueueId, Organization organization=Organization.JGI) {
        return getLibraryCreationQueues(organization).find { it.id as BigInteger == lcQueueId }
    }

    private void registerSpecs(Organization organization) {
        if(libraryCreationSpecsCvs && libraryCreationSpecsCvs[organization])
            return
        List<LibraryCreationSpecs> lcSpecsDtos = clarityDataService.getLibraryCreationSpecs(organization)
        libraryCreationSpecsCvs = lcSpecsDtos.collect {
            LibraryCreationSpecsMapper.toLibraryCreationSpecsCv(it)
        }.groupBy {it.organization }
    }
}
