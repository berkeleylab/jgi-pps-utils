package gov.doe.jgi.utils

import org.slf4j.MDC
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor

import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * Adds logging.
 */
@Component
class LoggingHandlerInterceptor implements HandlerInterceptor {
    /**
     * set of keys added to MDC so can be removed
     */
    //private ThreadLocal<Set<String>> storedKeys = ThreadLocal.withInitial({() {-> new HashSet<>()}});
    private ThreadLocal<Set<String>> storedKeys = ThreadLocal.withInitial({
        new HashSet<>()
    })

    static final String ZUUL_CONSUL_ID = 'zuul_consul_id'

    @Override
    boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //addKey("sessionId", request.getHeader("X-Session-Id"));
        //addKey("url", request.getRequestURI());
        if (request.getHeader(ZUUL_CONSUL_ID) != null) {
            addKey(ZUUL_CONSUL_ID, request.getHeader(ZUUL_CONSUL_ID))
        }
        return true
    }

    private void addKey(String key, String value) {
        MDC.put(key, value)
        storedKeys.get().add(key)
    }

//    @Override
//    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        // request ended on current thread remove properties
//        removeKeys();
//    }

    @Override
    void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        if (!ex)
            removeKeys()
    }

    private void removeKeys() {
        for (String key : storedKeys.get()) {
            MDC.remove(key)
        }
        storedKeys.remove()
    }
}
