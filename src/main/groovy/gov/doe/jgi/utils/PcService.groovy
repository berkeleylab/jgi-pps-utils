package gov.doe.jgi.utils

import gov.doe.jgi.domain.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

import java.time.Duration

@Service
class PcService {
    static final Logger log = LoggerFactory.getLogger(PcService.class)

    @Autowired
    WebClient wsAccessClient

    private Map<Long, DegreeOfPooling> degreeOfPoolingCached
    private Map<Long, SequencingStrategy> sequencingStrategiesCached
    private Map<String, SequencingStrategy> nameSequencingStrategiesCached
    private Map<Long, SequencingProduct> sequencingProductsCached
    private Map<Long, MaterialType> materialTypesCached
    private Map<Long, ItagPrimerSet> iTagCached
    private Map<Long, ExomeCaptureProbeSet> exomeCpsCached
    private Map<Integer, SowItemType> sowItemTypesCached
    private Map<Integer, FinalDelivProduct> finalDelivProductsCached
    private Map<Integer, LogicalAmountUnits> logicalAmountUnitsCached

//    List<ItagPrimerSet> getItagsFromSQL() {
//        ItagPrimerSetGorm.list()*.toItagPrimerSet()
//    }


    Collection<DegreeOfPooling> getDegreeOfPoolingAll(){
        fetchDegreeOfPooling().values()
    }
    DegreeOfPooling getDegreeOfPooling(Long id) {
        if (id == null)
            return null
        fetchDegreeOfPooling()[id]
    }


    private Map<Long, DegreeOfPooling> fetchDegreeOfPooling() {
        if (degreeOfPoolingCached)
            return degreeOfPoolingCached
        String path = "dw-pc-degree-of-pooling-cv/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<DegreeOfPooling> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<DegreeOfPooling>>() {}).timeout(Duration.ofSeconds(60)).block()

        degreeOfPoolingCached = new HashMap<>()
        content.each { DegreeOfPooling degreeOfPooling ->
            degreeOfPoolingCached[degreeOfPooling.id] = degreeOfPooling
        }

        log.debug "retrieved ${degreeOfPoolingCached?.size()} degreeOfPoolingCached"
        return degreeOfPoolingCached
    }

    SequencingStrategy getSequencingStrategy(Long id) {
        if (id == null)
            return null
        fetchSequencingStrategies()[id]
    }

    private Map<Long, SequencingStrategy>fetchSequencingStrategies() {
        if (sequencingStrategiesCached)
            return sequencingStrategiesCached
        String path = "dw-pc-sequencing-strategies/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<SequencingStrategy> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<SequencingStrategy>>() {}).timeout(Duration.ofSeconds(60)).block()

        sequencingStrategiesCached = new HashMap<>()
        nameSequencingStrategiesCached = new HashMap<>()
        content.each { SequencingStrategy sequencingStrategy ->
            sequencingStrategiesCached[sequencingStrategy.id] = sequencingStrategy
            nameSequencingStrategiesCached[sequencingStrategy.sequencingStrategyName] = sequencingStrategy
        }

        log.debug "retrieved ${sequencingStrategiesCached?.size()} sequencingStrategiesCached"
        return sequencingStrategiesCached
    }

    SequencingProduct getSequencingProduct(Long id) {
        if (id == null)
            return null
        fetchSequencingProducts()[id]
    }

    Collection<SequencingProduct> getSequencingProducts() {
            fetchSequencingProducts().values()
    }

    private Map<Long, SequencingProduct> fetchSequencingProducts() {
        if (sequencingProductsCached)
            return sequencingProductsCached
        String path = "dw-pc-sequencing-products/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<SequencingProduct> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<SequencingProduct>>() {}).timeout(Duration.ofSeconds(60)).block()

        sequencingProductsCached = new HashMap<>()
        content.each { SequencingProduct sequencingProduct ->
            sequencingProductsCached[sequencingProduct.id] = sequencingProduct
        }

        log.debug "retrieved ${sequencingProductsCached?.size()} sequencingProductsCached"
        return sequencingProductsCached
    }

    SequencingStrategy getSequencingStrategyByName(String stratName) {
        if (!nameSequencingStrategiesCached) {
            fetchSequencingStrategies()
        }
        assert nameSequencingStrategiesCached
        nameSequencingStrategiesCached[stratName]
    }

    private Map<Long, MaterialType> fetchMaterialTypes() {
        if (materialTypesCached)
            return materialTypesCached
        String path = "dw-pc-material-types-cv/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<MaterialType> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<MaterialType>>() {}).timeout(Duration.ofSeconds(60)).block()

        materialTypesCached = new HashMap<>()
        content.each { MaterialType materialType ->
            materialTypesCached[materialType.id] = materialType
        }

        log.debug "retrieved ${materialTypesCached?.size()} materialTypesCached"
        return materialTypesCached
    }

    MaterialType getMaterialType(Long id) {
        if (id == null)
            return null
        fetchMaterialTypes()[id]
    }

    ItagPrimerSet getItagPrimerSet(Long id) {
        if (id == null)
            return null
        fetchItagPrimerSet()[id]
    }

    Map<Long, ItagPrimerSet> fetchItagPrimerSet() {
        if (iTagCached)
            return iTagCached
        String path = "dw-pc-itag-primer-sets-cv/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<ItagPrimerSet> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<ItagPrimerSet>>() {}).timeout(Duration.ofSeconds(60)).block()

        iTagCached = new HashMap<>()
        content.each { ItagPrimerSet itagPrimerSet ->
            iTagCached[itagPrimerSet.id] = itagPrimerSet
        }

        log.debug "retrieved ${iTagCached?.size()} exomeCpsCached"
        return iTagCached
    }

    ExomeCaptureProbeSet getExomeCaptureProbeSet(Long id) {
        if (id == null)
            return null
        fetchExomeCaptureProbeSet()[id]
    }

    Map<Long, ExomeCaptureProbeSet> fetchExomeCaptureProbeSet() {
        if (exomeCpsCached)
            return exomeCpsCached
        String path = "dw-pc-exome-capture-probe-sets-cv/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<ExomeCaptureProbeSet> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<ExomeCaptureProbeSet>>() {}).timeout(Duration.ofSeconds(60)).block()

        exomeCpsCached = new HashMap<>()
        content.each { ExomeCaptureProbeSet exomeCaptureProbeSet ->
            exomeCpsCached[exomeCaptureProbeSet.id] = exomeCaptureProbeSet
        }

        log.debug "retrieved ${exomeCpsCached?.size()} exomeCpsCached"
        return exomeCpsCached
    }

    private Map<Integer, SowItemType> fetchSowItemTypes() {
        if (sowItemTypesCached)
            return sowItemTypesCached
        String path = "dw-pc-sow-item-types-cv/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<SowItemType> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<SowItemType>>() {}).timeout(Duration.ofSeconds(60)).block()

        sowItemTypesCached = new HashMap<>()
        content.each { SowItemType sowItemType ->
            sowItemTypesCached[sowItemType.id] = sowItemType
        }

        log.debug "retrieved ${sowItemTypesCached?.size()} sowItemTypesCached"
        return sowItemTypesCached
    }

    SowItemType getSowItemType(Long id) {
        if (id == null)
            return null
        fetchSowItemTypes()[id]
    }

    private Map<Integer, FinalDelivProduct> fetchFinalDelivProducts() {
        if (finalDelivProductsCached)
            return finalDelivProductsCached
        String path = "dw-pc-final-deliverable-products-cv/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<FinalDelivProduct> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<FinalDelivProduct>>() {}).timeout(Duration.ofSeconds(60)).block()

        finalDelivProductsCached = new HashMap<>()
        content.each { FinalDelivProduct finalDelivProduct ->
            finalDelivProductsCached[finalDelivProduct.id] = finalDelivProduct
        }

        log.debug "retrieved ${finalDelivProductsCached?.size()} finalDelivProductsCached"
        return finalDelivProductsCached
    }

    Map<Integer, LogicalAmountUnits> fetchLogicalAmountUnits() {
        if (logicalAmountUnitsCached)
            return logicalAmountUnitsCached
        String path = "dw-pc-logical-amount-units-cv/"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        List<LogicalAmountUnits> content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<List<LogicalAmountUnits>>() {}).timeout(Duration.ofSeconds(60)).block()

        logicalAmountUnitsCached = new HashMap<>()
        content.each { LogicalAmountUnits logicalAmountUnits ->
            logicalAmountUnitsCached[logicalAmountUnits.id] = logicalAmountUnits
        }

        log.debug "retrieved ${logicalAmountUnitsCached?.size()} logicalAmountUnitsCached"
        return logicalAmountUnitsCached
    }




    FinalDelivProduct getFinalDelivProduct(Long id) {
        if (id == null)
            return null
        fetchFinalDelivProducts()[id]
    }

    LogicalAmountUnits getLogicalAmountUnits(Long id) {
        if (id == null)
            return null
        fetchLogicalAmountUnits()[id]
    }
}

