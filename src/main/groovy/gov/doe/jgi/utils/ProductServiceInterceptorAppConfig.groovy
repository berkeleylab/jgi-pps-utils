package gov.doe.jgi.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Component
class ProductServiceInterceptorAppConfig  implements WebMvcConfigurer {// extends WebMvcConfigurerAdapter {
    @Autowired
    LoggingHandlerInterceptor loggingHandlerInterceptor

    @Override
    void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loggingHandlerInterceptor)
    }
}
