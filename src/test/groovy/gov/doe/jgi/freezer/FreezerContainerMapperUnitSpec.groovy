package gov.doe.jgi.freezer

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Test

class FreezerContainerMapperUnitSpec{
    def json = """
[
{"barcode":"0378697823","freezer-location":"SAMRobotics1111: tube rack TR-A0171: row 6: column 5"},
{"barcode":"0378697571","freezer-location":"Stirling: Shelf 2: Inventory Box 56: Slot 15"},
{"barcode":"0378697571","freezer-location":"SAMRobotics1111: tube rack TR-A0171: row 6: column 4"},
{"barcode":"27-462513","freezer-location":"SAMRobotics1111: plate rack 27-462513: shelf 12: rack column 1"},
{"barcode":"27-462513","freezer-location":null},
{"barcode":"27-462513","freezer-location":""},
{"barcode":"27-462514","freezer-location":null},
{"barcode":"27-462514","freezer-location":""},
{"barcode":"0342674522","freezer-location":"Location Unavailable"}
]"""
    @Test
    void "test toFreezerContainerList  extends Specification"() {
        setup:
            List<FreezerDto> dtoList = new ObjectMapper().readValue(json, FreezerDto[].class)
        when:
            List<FreezerContainer> containers = FreezerContainerMapper.toFreezerContainerList(dtoList)
        then:
            containers.size() == 5
            containers.each{ FreezerContainer container ->
                List<FreezerDto> freezerDtoList = dtoList.findAll{it.barcode == container.barcode}
                if (container.barcode == '0378697571') {
                    assert container.location == "${freezerDtoList[0].freezerLocation}; ${freezerDtoList[1].freezerLocation}"
                } else if (container.barcode == '27-462514') {
                    assert container.location == ""
                } else {
                    assert container.location == (freezerDtoList[0].freezerLocation == null ?'':freezerDtoList[0].freezerLocation)
                }
                assert container.barcode == freezerDtoList*.barcode.unique().first()
            }
        when:
            def result = FreezerContainerMapper.toFreezerContainerList([])
        then:
            assert !result
    }
}
