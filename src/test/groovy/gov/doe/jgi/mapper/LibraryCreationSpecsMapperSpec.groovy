package gov.doe.jgi.mapper

import com.fasterxml.jackson.databind.ObjectMapper
import gov.doe.jgi.config.Organization
import gov.doe.jgi.domain.LibraryCreationSpecs
import gov.doe.jgi.model.LibraryCreationSpecsCv
import org.junit.jupiter.api.Test

class LibraryCreationSpecsMapperSpec {
    static final String JGI = """{"lc-specs-id":1,"lc-specs":"Illumina Bisulphite-Seq","lc-specs-active":true,"poly-a-selection":false,"rrna-depletion":false,"target-template-size-bp":700.0,"tight-insert":false,"protocol-id":12,"organization-name":"JGI","lc-queues":[{"lc-queue-id":1,"lc-queue":"Illumina Bisulphite-Seq, Tubes","plate-target-vol-lib-trial-ul":null,"plate-target-mass-lib-trial-ng":null,"tube-target-vol-lib-trial-ul":50.0,"tube-target-mass-lib-trial-ng":50.0},{"lc-queue-id":74,"lc-queue":"Illumina Bisulphite-Seq, Plates","plate-target-vol-lib-trial-ul":140.0,"plate-target-mass-lib-trial-ng":140.0,"tube-target-vol-lib-trial-ul":null,"tube-target-mass-lib-trial-ng":null}]}"""
    static final String HUDSON_ALPHA = """{"lc-specs-id":90,"lc-specs":"Illumina, standard RNASeq w/PolyA Selection, 270bp","lc-specs-active":false,"poly-a-selection":false,"rrna-depletion":false,"target-template-size-bp":null,"tight-insert":false,"protocol-id":32,"organization-name":"Hudson Alpha","lc-queues":[]},{"lc-specs-id":91,"lc-specs":"Illumina, HiC Standard","lc-specs-active":false,"poly-a-selection":false,"rrna-depletion":false,"target-template-size-bp":null,"tight-insert":false,"protocol-id":33,"organization-name":"Hudson Alpha","lc-queues":[]}"""

    static LibraryCreationSpecs getLibraryCreationDto(String json) {
        ObjectMapper mapper = new ObjectMapper()
        return mapper.readValue(json, LibraryCreationSpecs.class)
    }

    static LibraryCreationSpecsCv getJgiSpecsCV() {
        return LibraryCreationSpecsMapper.toLibraryCreationSpecsCv(getLibraryCreationDto(JGI))
    }

    static LibraryCreationSpecsCv getHudsonAlphaSpecsCv() {
        return LibraryCreationSpecsMapper.toLibraryCreationSpecsCv(getLibraryCreationDto(HUDSON_ALPHA))
    }
    @Test
    void "test mapper" () {
        when:
            LibraryCreationSpecsCv libraryCreationSpecsCv = jgiSpecsCV
        then:
            assert libraryCreationSpecsCv.active
            assert libraryCreationSpecsCv.libraryCreationSpecs == "Illumina Bisulphite-Seq"
            assert libraryCreationSpecsCv.id == 1
            assert libraryCreationSpecsCv.libraryCreationQueues.size() == 2
            assert libraryCreationSpecsCv.organization == Organization.JGI
            libraryCreationSpecsCv.libraryCreationQueues.each {
                assert it.isTubeQueue || it.isPlateQueue
                assert it.libraryCreationSpecs == libraryCreationSpecsCv
                assert it.libraryCreationQueue in ["Illumina Bisulphite-Seq, Tubes", "Illumina Bisulphite-Seq, Plates"]
            }
        when:
            libraryCreationSpecsCv = hudsonAlphaSpecsCv
        then:
            assert !libraryCreationSpecsCv.active
            assert libraryCreationSpecsCv.libraryCreationSpecs == "Illumina, standard RNASeq w/PolyA Selection, 270bp"
            assert libraryCreationSpecsCv.id == 90
            assert !libraryCreationSpecsCv.libraryCreationQueues
            assert libraryCreationSpecsCv.organization == Organization.HUDSON_ALPHA
    }
}
