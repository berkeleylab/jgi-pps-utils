package gov.doe.jgi.mapper

import com.fasterxml.jackson.databind.ObjectMapper
import gov.doe.jgi.domain.QcType
import gov.doe.jgi.model.QcTypeCv
import org.junit.jupiter.api.Test

class QcTypeCvMapperUnitSpec {
    def json = """
[
{
"qc-type-id": 0,
"qc-type": "None",
"active": true
},
{
"qc-type-id": 1,
"qc-type": "Purity",
"active": true
},
{
"qc-type-id": 231,
"qc-type": "Quantity+Quality+Purity+manual Quantity+manual Quality+manual Purity",
"active": true
}
]
"""
    @Test
    void "test toQcTypeCvList"() {
        setup:
            List<QcType> qcTypeDtoList = new ObjectMapper().readValue(json, QcType[].class)
        when:
            List<QcTypeCv> qcTypes = QcTypeCvMapper.toQcTypeCvList(qcTypeDtoList)
        then:
            assert qcTypes.size() == 3
            qcTypes.each{
                assert it.qcType
                assert it.id >= 0
                assert it.active in [Boolean.TRUE, Boolean.FALSE]
            }
    }
    @Test
    void "test toQcTypeCv"() {
        setup:
            List<QcType> qcTypeDtoList = new ObjectMapper().readValue(json, QcType[].class)
            def dto1 = qcTypeDtoList[1]
        when:
            QcTypeCv qcTypeCv1 = QcTypeCvMapper.toQcTypeCv(dto1)
        then:
            assert qcTypeCv1.active == (dto1.active == 'Y')
            assert qcTypeCv1.id == dto1.id
            assert qcTypeCv1.qcType == dto1.qcType
        when:
            def obj = QcTypeCvMapper.toQcTypeCv(null)
        then:
            assert obj == null
    }
}
