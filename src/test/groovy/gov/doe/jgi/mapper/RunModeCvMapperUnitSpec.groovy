package gov.doe.jgi.mapper

import com.fasterxml.jackson.databind.ObjectMapper
import gov.doe.jgi.domain.RunMode
import gov.doe.jgi.model.RunModeCv
import org.junit.jupiter.api.Test

class RunModeCvMapperUnitSpec {
    def json = """
[
{
"run-mode-id": 1,
"run-mode": "1R Short",
"read-total": 1,
"read-length-bp": 100,
"estimated-duration-days": 0.0,
"raw-data-output-gb-channel": null,
"raw-reads-m-channel": null,
"estimated-cost-per-channel-flo": null,
"estimated-cost-per-gb": null,
"estimated-cost-per-m-reads": null,
"clarity-lims-queue-name": null,
"sequencer-model-id": 1,
"sequencer-model": "HiSeq",
"platform-id": 1,
"platform": "Illumina",
"organization-name": "JGI",
"active": false
},
{
    "run-mode-id": 57,
    "run-mode": "Illumina NovaSeq S4 2 X 150",
    "read-total": 2,
    "read-length-bp": 150,
    "estimated-duration-days": null,
    "raw-data-output-gb-channel": null,
    "raw-reads-m-channel": null,
    "estimated-cost-per-channel-flo": null,
    "estimated-cost-per-gb": null,
    "estimated-cost-per-m-reads": null,
    "clarity-lims-queue-name": "SQ Sequencing Illumina NovaSeq",
    "sequencer-model-id": 18,
    "sequencer-model": "NovaSeq S4",
    "platform-id": 1,
    "platform": "Illumina",
    "organization-name": "JGI",
    "active": true
},
{
    "run-mode-id": 61,
    "run-mode": "Illumina NovaSeq 1 X 125",
    "read-total": 1,
    "read-length-bp": 125,
    "estimated-duration-days": null,
    "raw-data-output-gb-channel": null,
    "raw-reads-m-channel": null,
    "estimated-cost-per-channel-flo": null,
    "estimated-cost-per-gb": null,
    "estimated-cost-per-m-reads": null,
    "clarity-lims-queue-name": "SQ Sequencing Illumina X10",
    "sequencer-model-id": 14,
    "sequencer-model": "NovaSeq",
    "platform-id": 1,
    "platform": "Illumina",
    "organization-name": "Hudson Alpha",
    "active": false
}
]
"""
    @Test
    void "test toRunModeCvList"() {
        setup:
        List<RunMode> dtoList = new ObjectMapper().readValue(json, RunMode[].class)
        when:
        List<RunModeCv> runModeCvs = RunModeCvMapper.toRunModeCvList(dtoList)
        then:
            assert runModeCvs.size() == 3
            runModeCvs.each{
                assert it.runMode
                assert it.id
                assert it.active in [Boolean.TRUE, Boolean.FALSE]
            }
    }
    @Test
    void "test toRunModeCv"() {
        setup:
            List<RunMode> dtoList = new ObjectMapper().readValue(json, RunMode[].class)
        when:
            def dto0 = dtoList[0]
            RunModeCv runModeCv0 = RunModeCvMapper.toRunModeCv(dto0)
        then:
            assert runModeCv0.organizationName == dto0.organizationName
            assert runModeCv0.active == (dto0.active == 'Y')
            assert runModeCv0.id == dto0.id
            assert runModeCv0.runMode == dto0.runMode
            assert runModeCv0.sequencerModel.id == dto0.sequencerModelId
            assert runModeCv0.sequencerModel.sequencerModel == dto0.sequencerModel
            assert runModeCv0.sequencerModel.platformCv.platform == dto0.platform
            assert runModeCv0.sequencerModel.platformCv.id == dto0.platformId
            assert runModeCv0.clarityLimsQueueName == dto0.workflowName
            assert runModeCv0.readLengthBp == dto0.readLengthBp
            assert runModeCv0.readTotal == dto0.readTotal
        when:
            def obj = RunModeCvMapper.toRunModeCv(null)
        then:
            assert obj == null
    }
}
