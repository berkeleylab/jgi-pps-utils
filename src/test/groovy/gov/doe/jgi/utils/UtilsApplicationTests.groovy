package gov.doe.jgi.utils

import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class UtilsApplicationTests {
    Logger logger = LoggerFactory.getLogger(UtilsApplicationTests.class.name)

    @Test
    void contextLoads() {
        logger.error "ERROR LOG LEVEL TEST"
        logger.info "INFO LOG LEVEL TEST"
        logger.debug "DEBUG LOG LEVEL TEST"
    }

}
